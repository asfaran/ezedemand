<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodBandwidthsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bod_bandwidth', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('bod_name')->nullable();
			$table->string('price')->nullable();
			$table->integer('value')->nullable()->default(0);
			$table->integer('max_value')->nullable()->default(0);
			$table->integer('type')->nullable()->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bod_bandwidth');
	}

}
