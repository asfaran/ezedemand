<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bod_lines', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('alias_id')->nullable()->default(0);
			$table->string('account_no')->nullable();
			$table->string('interface')->nullable();
			$table->integer('down_default')->nullable()->default(0);
			$table->integer('down_max')->nullable()->default(0);
			$table->integer('up_default')->nullable()->default(0);
			$table->integer('up_max')->nullable()->default(0);
			$table->string('host_mac')->nullable();
			$table->string('client_mac')->nullable();
			$table->integer('status')->nullable()->default(0);
			$table->timestamps('lastseen')->default('CURRENT_TIMESTAMP');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::drop('bod_lines');
	}

}
