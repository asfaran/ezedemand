<?php namespace App\Http\Controllers;

use App\Bod_request;
use App\Gateways;
use Carbon;
use App\Http\Requests;
use App\Default_bandwidth;
use App\Action_log;
use App\Lines;
use DB;
use Session;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class GatewayController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}


	/**
	 * Display a listing of the Gateway.
	 *
	 * @return Response
	 */
	public function index($id=NULL)
	{
		if($id == null ){
			if(Auth::user()->access == 2) {
				$gateway_list = DB::table('lwalias')
					->leftJoin('groups', 'groups.id', '=', 'lwalias.groupid')
					->select('lwalias.*', 'groups.name')
					->get();
			}elseif(Auth::user()->access == 1){
				$gateway_list = DB::table('lwalias')
					->leftJoin('groups', 'groups.id', '=', 'lwalias.groupid')
					->select('lwalias.*', 'groups.name')
					->where('lwalias.groupid', '=', Auth::user()->groupid )
					->get();

			}else{
				$gateway_list = DB::table('lwalias')
					->leftJoin('groups', 'groups.id', '=', 'lwalias.groupid')
					->select('lwalias.*', 'groups.name')
					->where('lwalias.id', '=', Auth::user()->aliasid )
					->get();

			}
				return view('gateway.gateway', ['gateway_list' => $gateway_list,'url_id' => $id]);

		}else{
			$today = Carbon\Carbon::now()->toDateTimeString();

				if(Auth::user()->access == 2) {
					$query = DB::table('lwalias')
						->leftJoin('groups', 'groups.id', '=', 'lwalias.groupid')
						->select('lwalias.*', 'groups.name',DB::raw('TIMESTAMPDIFF(MINUTE,lwalias.updated_at,"'.$today.'")  as date_diff'));
				}elseif(Auth::user()->access == 1){
					$query = DB::table('lwalias')
						->leftJoin('groups', 'groups.id', '=', 'lwalias.groupid')
						->select('lwalias.*', 'groups.name',DB::raw('TIMESTAMPDIFF(MINUTE,lwalias.updated_at,"'.$today.'")  as date_diff'))
						->where('lwalias.groupid', '=', Auth::user()->groupid );

				}else{
					$query = DB::table('lwalias')
						->leftJoin('groups', 'groups.id', '=', 'lwalias.groupid')
						->select('lwalias.*', 'groups.name',DB::raw('TIMESTAMPDIFF(MINUTE,lwalias.updated_at,"'.$today.'")  as date_diff'))
						->where('lwalias.id', '=', Auth::user()->aliasid );

				}


			if($id == 1){
				$gateway_list =$query->where(DB::raw('TIMESTAMPDIFF(MINUTE,lwalias.updated_at,"'.$today.'")'), '<=', 5)->get();

			}elseif($id == '0'){
				$gateway_list =$query->where(DB::raw('TIMESTAMPDIFF(MINUTE,lwalias.updated_at,"'.$today.'")'), '>', 5)->get();
			}else{
				$gateway_list =$query->get();

			}


				return view('gateway.gateway', ['gateway_list' => $gateway_list,'url_id' => $id]);

		}


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Ajax Return Function for get max download bandwidth value.
	 *
	 * @return Response
	 */
	public function lines_maxdown()
	{

		$bandwidth = DB::table('default_bandwidth')->where('id', '=', $_POST['default_down'])->first();

		return view('gateway.maxbandwidth', ['bandwidth' => $bandwidth]);

	}


	/**
	 * Ajax Return Function for get max upload bandwidth value.
	 *
	 * @return Response
	 */
	public function lines_maxup()
	{

		$bandwidth = DB::table('default_bandwidth')->where('id', '=', $_POST['default_up'])->first();

		return view('gateway.maxbandwidth', ['bandwidth' => $bandwidth]);

	}


	/**
	 * Ajax Return Function for list of lines in select drop down.
	 *
	 * @return Response
	 */
	public function load_linelist()
	{

		$lines_list = DB::table('bod_lines')
			->leftJoin('default_bandwidth as default_up', 'default_up.id', '=', 'bod_lines.up_default')
			->leftJoin('default_bandwidth as default_down', 'default_down.id', '=', 'bod_lines.down_default')
			->select('bod_lines.*', 'default_up.name as up_name', 'default_down.name as down_name')
			->where('bod_lines.alias_id', '=', $_POST['alias_id'])->get();

		return view('gateway.load_linelist', ['line_list' => $lines_list]);

	}


	/**
	 * Ajax Return Function for list cities in select drop down.
	 *
	 * @return Response
	 */
	public function loadcities()
	{

		$city_list = DB::table('city')->where('country_id', '=', $_POST['country_id'])->get();

		if($city_list){
			$city_array[0] ="[Select a city]";
			foreach($city_list  as $city){
				$city_array[$city->id]= $city->city_name;

			}
		}else{
			$city_array[0] ="[No cities listed for this country]";
		}

		return view('gateway.load_cities', ['city_array' => $city_array]);

	}



	/**
	 * Show the form for editing the specified Gateway.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit_gateway($id)
	{
		$lines_list = DB::table('bod_lines')->where('alias_id', '=', $id)->get();

		$bandwidth_list = DB::table('default_bandwidth')->select('id', 'name')->get();
		$bandwidth_array[0] ="[Select a Bandwidth]";
		foreach($bandwidth_list  as $bandwidth){
			$bandwidth_array[$bandwidth->id]= $bandwidth->name;

		}


		$group_list = DB::table('groups')->select('id', 'name')->get();
		$group_array[0] ="[Select a Group]";
		foreach($group_list  as $group){
			$group_array[$group->id]= $group->name;

		}

		$country_list = DB::table('countries')->select('id', 'country_name')->get();
		$country_array[0] ="[Select a Country]";
		foreach($country_list  as $country){
			$country_array[$country->id]= $country->country_name;

		}

		$city_list = DB::table('city')->select('id', 'city_name')->get();
		$city_array[0] ="[Select a City]";
		foreach($city_list  as $city){
			$city_array[$city->id]= $city->city_name;

		}

		$gateway_data = DB::table('lwalias')
			->leftJoin('city', 'city.id', '=', 'lwalias.cityid')
			->select('lwalias.*' , 'city.country_id as country')
			->where('lwalias.id', '=', $id)
			->first();


		return view('gateway.edit_gateway',
							[
								'gateway_data' => $gateway_data,
								'group_array' => $group_array,
								'line_list' => $lines_list,
								'bandwidth_array' => $bandwidth_array,
								'city_array' => $city_array,
								'country_array' => $country_array
							]);


	}


	/**
	 * store Updated the specified gateway.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update_gateway(Request $request)
	{

		$v = Validator::make($request->all(), [
			'name' => 'required',
			'mac' => 'required',
			'groupid' => 'required|Numeric',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{
			$gateway = Gateways::find($request->input('id'));
			$gateway->alias = $request->input('name');
			$gateway->mac = $request->input('mac');
			$gateway->groupid = $request->input('groupid');
			$gateway->cityid = $request->input('city');
			$gateway->building = $request->input('building');
			$gateway->street = $request->input('street');
			$gateway->latitude = $request->input('latitude');
			$gateway->longitude = $request->input('longitude');

			$gateway->save();


			if($request->input('line_data')){
				foreach($request->input('line_data') as $key => $value ){
					//foreach($values as  $value ) {
					$down_array = DB::table('default_bandwidth')->where('id', '=', $value['down_default'])->first();
					$up_array = DB::table('default_bandwidth')->where('id', '=', $value['up_default'])->first();

					if(isset($_POST['status'])){  $status= 1; }else{ $status= 0; };




						$lines = Lines::find($key);
						$lines->alias_id = $request->input('id');
						$lines->account_no = $value['account_no'];
						$lines->down_default = $value['down_default'];
						$lines->down_max = ($down_array->value)*4;
						$lines->up_default = $value['up_default'];
						$lines->up_max = ($up_array->value)*4;
						$lines->status =$status;
						$lines->save();
					//}
				}

			}


			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Gateway Updated successfully';
			$action_log->save();
			/* action log insertion */



			return redirect('gateway')->with('flash_success', 'Gateway Updated successfully!.');

		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy_gateway($id)
	{

		$affectedRows = Gateways::where('id', '=', $id)->delete();
		if($affectedRows){
			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Gateway Deleted  ';
			$action_log->save();
			/* action log insertion */
			$bodRows = Bod_request::where('user_id', '=', $id)->delete();
			$lineRows = DB::table('bod_lines')->where('alias_id', '=', $id)->delete();

			if($bodRows  || $lineRows){
				$action_log = new Action_log();
				$action_log->userid = Auth::user()->id;
				$action_log->username = Auth::user()->name;
				$action_log->action = ' Bandwidth Requests and Interfaces also delete related to Deleted Gateway';
				$action_log->save();
			}

			/* action log insertion */




			return redirect('gateway')->with('flash_success', 'Gateway & Related Data deleted successfully!.');
		}else{
			return redirect('gateway')->with('flash_message', 'Gateway not deleted,Try again later ....');
		}
	}

}
