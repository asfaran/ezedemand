<?php namespace App\Http\Controllers;

use App\Bod_request;
use File;
use DB;
Use Validator;
use Auth;
use Session;
use PDF;
use Config;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ReportController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function reportbr()
	{
		$group_list = DB::table('groups')->select('id', 'name')->get();
		$group_array[0] ="[Select a Group]";
		foreach($group_list  as $group){
			$group_array[$group->id]= $group->name;

		}
		return view('reports.reportbr', ['group_array' => $group_array]);
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function bdmonitor()
	{
		if(Auth::user()->access == 2 ){
			$gateway_list = DB::table('lwalias')->select('id', 'alias')->get();
		}else{
			$gateway_list = DB::table('lwalias')->select('id', 'alias')->where('groupid', '=', Auth::user()->groupid )->get();
		}


		if($gateway_list){
			$gateway_array[0] ="[Select a Bandwidth]";
			foreach($gateway_list  as $gateway){
				$gateway_array[$gateway->id]= $gateway->alias;

			}
		}else{
			$gateway_array[0] ="[No Gateway for this Group]";
		}

		$group_list = DB::table('groups')->select('id', 'name')->get();
		$group_array[0] ="[Select a Group]";
		foreach($group_list  as $group){
			$group_array[$group->id]= $group->name;

		}
		return view('reports.bdmonitor', ['group_array' => $group_array,'gateway_array' => $gateway_array]);
	}


	public function load_period()
	{

		return view('reports.load_period', ['report_type' => $_POST['report_type']]);
	}

	public function generate_reportbr(Request $request)
	{

		if($request->input('report_types') == 'usage' || $request->input('report_types') == 'revenue'){
			$v = Validator::make($request->all(), [
				'user_id' => 'required|integer',
				'report_types' => 'required',
				'month' => 'required',

			]);
			$month = $request->input('month');

			$filter_date_start = $month."-01 00:00";
			$filter_date_end = $month."-31 23:59";

			$start_date = date('Y-m-d H:i:s', strtotime($filter_date_start));
			$end_date = date('Y-m-d H:i:s', strtotime($filter_date_end));
			$report_name = "Bandwidth Request(".$request->input('report_types').") Report For $month";
			$rtype= $request->input('report_types')." Report";
			$period = "monthly report for ".$month;

		}elseif($request->input('report_types') == 'subscriber'){

			$filter_date_start = $request->input('date_start');
			$filter_date_end = $request->input('date_end');
			$report_name = "Bandwidth Request(Subscriber Report) - ".$filter_date_start." to ".$filter_date_end;

			$start_date = date('Y-m-d H:i:s', strtotime($filter_date_start));
			$end_date = date('Y-m-d H:i:s', strtotime($filter_date_end));
			$rtype= "Subscriber Report";
			$period = date('Y/M/d', strtotime($start_date))."  <strong>To</strong>  ".date('Y/M/d', strtotime($end_date));


			$v = Validator::make($request->all(), [
				'user_id' => 'required|integer',
				'report_types' => 'required',
				'date_start' => 'required',
				'date_end' => 'required',

			]);


		}else{

			$v = Validator::make($request->all(), [
				'user_id' => 'required|integer',
				'line_id' => 'required|integer',
				'report_types' => 'required',

			]);
		}

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{
			$line_id = $request->input('line_id');
			$user_id =$request->input('user_id');
			$group_list =$request->input('groupid');

			$group_row = DB::table('groups')->where('id', '=', $group_list)->first();
			$group_name = $group_row->name;
			if($user_id){
				$customer_row = DB::table('lwalias')->where('id', '=', $user_id)->first();
				$customer_name = $customer_row->alias;
			}else{
				$customer_name ="";

			}




			$query = DB::table('bod_request')
							->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
							->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
							->leftJoin('groups', 'groups.id', '=', 'lwalias.groupid')
							->leftJoin('bod_bandwidth as bdd', 'bdd.id', '=', 'bod_request.bandwidth_down')
							->leftJoin('bod_bandwidth as bdu', 'bdu.id', '=', 'bod_request.bandwidth_up')
							->select('bod_request.date_created' ,
								'bod_lines.account_no as accno',
								'lwalias.alias as client_name',
								'bod_request.date_created as Subscription_Activation_Date' ,
								'bod_request.date_created' ,
								'bdd.bod_name',
								'bod_request.bandwidth_down',
								'bod_request.date_end as date_end' ,
								'bod_request.date_start as date_start',
								'bod_request.date_end as deactivation_date',
								'bdd.price as Unit_Price',
								'bod_request.burst',
								'bod_request.id',
								'groups.name as group_name',
								'bod_lines.interface as interface')
							->where('bod_request.date_start', '>=', $filter_date_start)
							->where('bod_request.date_end', '<=', $filter_date_end);


				if($line_id > 0 ){
					$request_list =$query->where('bod_request.line_id','=',$line_id)->groupBy('bod_request.id')->get();;
				}elseif((!$line_id || $line_id == 0 ) && $user_id > 0 ){
					$request_list =$query->where('bod_request.user_id','=',$user_id)->groupBy('bod_request.id')->get();;
				}elseif((!$line_id || $line_id == 0 ) && (!$user_id || $user_id == 0 ) && $group_list > 0 ){
					$request_list =$query->where('lwalias.groupid','=',$group_list)->groupBy('bod_request.id')->get();;
				}else{
					$request_list =$query->groupBy('bod_request.id')->get();;
				}



			return view('reports.result_br',
				['request_list' => $request_list ,
					'group_name' => $group_name ,
					'user_id' => $user_id ,
					'line_id' => $line_id ,
					'group_id' => $group_list ,
					'customer_name' => $customer_name  ,
					'period' => $period ,
					'rtype' => $rtype ,
					'report_name' => $report_name ,
					'start_date' => $filter_date_start ,
					'end_date' => $filter_date_end ,
					'report_type' => $request->input('report_types')
				]);
		}


	}


	public function generate_bdmonitor(Request $request)
	{
		if(Auth::user()->access == 2) {
			$v = Validator::make($request->all(), [
				'groupid' => 'required|integer',
				'report_types' => 'required',

			]);
		}else{
			$v = Validator::make($request->all(), [
				'user_id' => 'required|integer',
				'report_types' => 'required',

			]);
		}

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{

			if($request->input('report_types') == 'hours'){

				$m= date("m");
				$de= date("d");
				$y= date("Y");
				for($i=1; $i<=24; $i++){
					$etc= date('d-m-y',mktime(0,0,0,$m,($de-$i),$y));
					$label[]= date('D-d/F',mktime(0,0,0,$m,($de-$i),$y));
					$check_label[]= (int) date('d',mktime(0,0,0,$m,($de-$i),$y));
				}
				$start_date = date('y-m-d',mktime(0,0,0,$m,($de-7),$y))." 00:00";
				$end_date = date('y-m-d',mktime(0,0,0,$m,($de-1),$y))." 23:59";


			}elseif($request->input('report_types') == 'dailyweek'){

				$title = "Daily for week";

				$m= date("m");
				$de= date("d");
				$y= date("Y");
				for($i=1; $i<=7; $i++){
					$etc= date('d-m-y',mktime(0,0,0,$m,($de-$i),$y));
					$label[]= date('D-d/F',mktime(0,0,0,$m,($de-$i),$y));
					$check_label[]= (int) date('d',mktime(0,0,0,$m,($de-$i),$y));
				}
				$start_date = date('y-m-d',mktime(0,0,0,$m,($de-7),$y))." 00:00";
				$end_date = date('y-m-d',mktime(0,0,0,$m,($de-1),$y))." 23:59";


			}elseif($request->input('report_types') == 'dailymonth'){

				$title = "Daily for month ";

				$m= date("m");
				$de= date("d");
				$y= date("Y");
				for($i=1; $i<=30; $i++){
					$etc= date('d-m-y',mktime(0,0,0,$m,($de-$i),$y));
					$label[]= date('D-d/F',mktime(0,0,0,$m,($de-$i),$y));
					$check_label[]= (int) date('d',mktime(0,0,0,$m,($de-$i),$y));
				}
				$start_date = date('y-m-d',mktime(0,0,0,$m,($de-30),$y))." 00:00";
				$end_date = date('y-m-d',mktime(0,0,0,$m,($de-1),$y))." 23:59";


			}elseif($request->input('report_types') == 'monthly'){
				$title = "Monthly";

				for ($i = 1; $i <= 12; ++$i) {
					$time = strtotime(sprintf('-%d months', $i));
					$value = date('Y-m', $time);
					$label[] = date('F Y', $time);
					$check_label[]= (int) date('m', $time);
				}

				$start_date = date('Y-m', strtotime(sprintf('-%d months', 12)))."-01 00:00";
				$end_date =   date('Y-m', strtotime(sprintf('-%d months',  1)))."-31 23:59";


			}elseif($request->input('report_types') == 'yearly'){
				$title = "Yearly";

				for ($i = 1; $i <= 4; ++$i) {
					$year = date("Y");
					$label[] = $year - $i;
					$check_label[]= $year - $i;
				}

				$start_date = (date("Y")-4)."-01-01 00:00";
				$end_date =   (date("Y")-1)."-12-31 23:59";


			}


			$result_dynamic = array ();
			$result_static = array ();


			foreach($check_label AS $labels){
				$result_static[$labels] = 0;
				$result_dynamic[$labels] = 0;

			}

			/*

			->where('traffic_log.date_start', '>=', "$start_date")
					->where('traffic_log.date_end', '>=', "$end_date")
			*/


			$query = DB::table('traffic_log')
				->leftJoin('bod_lines', 'bod_lines.client_mac', '=', 'traffic_log.mac_address')
				->leftJoin('lwalias', 'lwalias.id', '=', 'bod_lines.alias_id')
				->leftJoin('interfaces', 'interfaces.line', '=', 'bod_lines.interface')
				->leftJoin('bod_request', function($join)
				{
					$join->on('bod_request.line_id', '=', 'bod_lines.id');
					$join->on('bod_request.user_id','=','lwalias.id');
				});


			if($request->input('report_types') == 'hours'){
				$query->select(DB::raw('round(MAX(traffic_log.bandwidth_down)/1024) as download'),
					DB::raw('round(MAX(traffic_log.bandwidth_up)/1024) as upload'),
					DB::raw('DAY(traffic_log.date_start) as  startdate'),
					DB::raw('DAY(traffic_log.date_end) as enddate')
				);

			}elseif($request->input('report_types') == 'dailyweek'){
				$query->select(DB::raw('round(MAX(traffic_log.bandwidth_down)/1024) as download'),
					DB::raw('round(MAX(traffic_log.bandwidth_up)/1024) as upload'),
					DB::raw('DAY(traffic_log.date_start) as  startdate'),
					DB::raw('DAY(traffic_log.date_end) as enddate')

				);

			}elseif($request->input('report_types') == 'dailymonth'){
				$query->select(DB::raw('round(MAX(traffic_log.bandwidth_down)/1024) as download'),
					DB::raw('round(MAX(traffic_log.bandwidth_up)/1024) as upload'),
					DB::raw('DAY(traffic_log.date_start) as  startdate'),
					DB::raw('DAY(traffic_log.date_end) as enddate')

				);

			}elseif($request->input('report_types') == 'monthly'){
				$query->select(DB::raw('round(MAX(traffic_log.bandwidth_down)/1024) as download'),
					DB::raw('round(MAX(traffic_log.bandwidth_up)/1024) as upload'),
					DB::raw('MONTH(traffic_log.date_start) as  startdate'),
					DB::raw('MONTH(traffic_log.date_end) as enddate')

				);

			}elseif($request->input('report_types') == 'yearly'){
				$query->select(DB::raw('round(MAX(traffic_log.bandwidth_down)/1024) as download'),
					DB::raw('round(MAX(traffic_log.bandwidth_up)/1024) as upload'),
					DB::raw('YEAR(traffic_log.date_start) as  startdate'),
					DB::raw('YEAR(traffic_log.date_end) as enddate')

				);

			}
			////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if($request->input('line_id') && $request->input('line_id') != 0 ){
				$query->where('bod_request.line_id', '=', $request->input('line_id'));
			}elseif((!$request->input('line_id') || $request->input('line_id') == 0) && $request->input('user_id')){
				$query->where('lwalias.id', '=', $request->input('user_id'));
			}elseif((!$request->input('line_id') || $request->input('line_id') == 0) && (!$request->input('user_id') || $request->input('user_id') == 0) && $request->input('groupid')){
				$query->where('lwalias.groupid', '=', $request->input('groupid'));
			}




			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




			if($request->input('report_types') == 'hours'){

				$static_list= $query->where('bod_request.burst', '=', 0)
					->groupBy(DB::raw('DATE(traffic_log.date_start)'))
					->orderBy('traffic_log.id', 'desc')
					->get();
				$dynamic_list= $query->where('bod_request.burst', '=', 1)
					->groupBy(DB::raw('DATE(traffic_log.date_start)'))
					->orderBy('traffic_log.id', 'desc')
					->get();

			}elseif($request->input('report_types') == 'dailyweek'){
				$static_list= $query->where('bod_request.burst', '=', 0)
					->groupBy(DB::raw('DATE(traffic_log.date_start)'))
					->orderBy('traffic_log.id', 'desc')
					->get();
				$dynamic_list= $query->where('bod_request.burst', '=', 1)
					->groupBy(DB::raw('DATE(traffic_log.date_start)'))
					->orderBy('traffic_log.id', 'desc')
					->get();

			}elseif($request->input('report_types') == 'dailymonth'){
				$static_list= $query->where('bod_request.burst', '=', 0)
					->groupBy(DB::raw('DATE(traffic_log.date_start)'))
					->orderBy('traffic_log.id', 'desc')
					->get();
				$dynamic_list= $query->where('bod_request.burst', '=', 1)
					->groupBy(DB::raw('DATE(traffic_log.date_start)'))
					->orderBy('traffic_log.id', 'desc')
					->get();

			}elseif($request->input('report_types') == 'monthly'){
				$static_list= $query->where('bod_request.burst', '=', 0)
					->groupBy(DB::raw('MONTH(traffic_log.date_start)'))
					->orderBy('traffic_log.date_start', 'desc')
					->get();
				$dynamic_list= $query->where('bod_request.burst', '=', 1)
					->groupBy(DB::raw('MONTH(traffic_log.date_start)'))
					->orderBy('traffic_log.date_start', 'desc')
					->get();

			}elseif($request->input('report_types') == 'yearly'){
				$static_list= $query->where('bod_request.burst', '=', 0)
					->groupBy(DB::raw('YEAR(traffic_log.date_start)'))
					->orderBy('traffic_log.id', 'desc')
					->get();
				$dynamic_list= $query->where('bod_request.burst', '=', 1)
					->groupBy(DB::raw('YEAR(traffic_log.date_start)'))
					->orderBy('traffic_log.id', 'desc')
					->get();

			}
			//dd($static_list);




			foreach($static_list  as $static){
				$result_static[$static->startdate] = $static->download;

			}

			foreach($dynamic_list  as $dynamic){
				$result_dynamic[$dynamic->startdate] = $dynamic->download;

			}


			return view('reports.result_monitor',
				[
					'report_types' => $request->input('report_types'),
					'groupid' => $request->input('groupid'),
					'label' => $label,
					'result_dynamic' => $result_dynamic,
					'result_static' => $result_static,
					'test_result' => $static_list
				]);
		}


	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function export_csv()
	{
		$unique_id = rand(1, 1000000);

		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=report-'.$unique_id.'.csv');
		$output = fopen('php://output', 'w');
		fputcsv($output, array('Bill_Date', 'Cust_Code', 'Subscription_Activation_Date',
			'Subscription_Deactivation_Date', 'Bill Cycle',
			'Product Code', 'Product Description', 'Quantity', 'Unit_Price',
			'Product_Total_Price', 'Product_Discount', 'Product_Total_Price_With_Discount',
			'Total_Price', 'Total_discount', 'Total_Price_With_Discount'));

		$start_date =  $_POST['start_date'];
		$end_date = $_POST['end_date'];
		$line_id = $_POST['line_id'];
		$user_id = $_POST['user_id'];
		$group_id = $_POST['group_id'];
		$result_array = array();


		$query = DB::table('bod_request')
			->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
			->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
			->leftJoin('groups', 'groups.id', '=', 'lwalias.groupid')
			->leftJoin('bod_bandwidth as bdd', 'bdd.id', '=', 'bod_request.bandwidth_down')
			->leftJoin('bod_bandwidth as bdu', 'bdd.id', '=', 'bod_request.bandwidth_up')
			->leftJoin('admins', 'admins.id', '=', 'bod_request.admin_id')
			->select('bod_request.date_created',
				'bod_lines.account_no',
				'bod_request.date_created as Subscription_Activation_Date',
				'bod_request.date_end',
				'lwalias.alias',
				'bod_request.bandwidth_down',
				'bdd.bod_name',
				'bod_request.date_start',
				'bod_request.date_end as deactivation_date',
				'bdd.price as Unit_Price',
				'bod_request.burst',
				'bod_request.id',
				'bod_request.line_id',
				'bod_request.user_id',
				'lwalias.groupid')
			->where('bod_request.date_start', '>=', $start_date)
			->where('bod_request.date_end', '<=', $end_date);



		if($line_id > 0 ){
			$request_list =$query->where('bod_request.line_id','=',$line_id)->groupBy('bod_request.id')->get();;
		}elseif((!$line_id || $line_id == 0 ) && $user_id > 0 ){
			$request_list =$query->where('bod_request.user_id','=',$user_id)->groupBy('bod_request.id')->get();;
		}elseif((!$line_id || $line_id == 0 ) && (!$user_id || $user_id == 0 ) && $group_id > 0 ){
			$request_list =$query->where('lwalias.groupid','=',$group_id)->groupBy('bod_request.id')->get();;
		}else{
			$request_list =$query->groupBy('bod_request.id')->get();;
		}


		foreach($request_list  as $result){

			$discount = 0;
			$diff = abs(strtotime($result->date_end) - strtotime($result->date_start));

			if ($result->burst == 1) {
				$quantity = floor($diff / (60 * 60));
				$total_price = $quantity * $result->Unit_Price;
				$bill_cycle ="Hourly";
				$type="Dynamic";
			} else {
				$quantity = floor($diff / (60 * 60 * 24));
				$total_price = $quantity * $result->Unit_Price;
				$bill_cycle ="Daily";
				$type="Static";
			}

			$result_array['Bill_Date'] = $result->date_created;
			$result_array['Cust_Code'] = $result->account_no;
			$result_array['Subscription_Activation_Date'] = $result->Subscription_Activation_Date;
			$result_array['Subscription_Deactivation_Date'] = $result->date_end;
			$result_array['Bill Cycle'] = $bill_cycle;
			$result_array['Product Code'] = $result->bandwidth_down;
			$result_array['Deactivation Date'] = $result->deactivation_date;
			$result_array['Quantity'] = $quantity;
			$result_array['Unit_Price'] = $result->Unit_Price;
			$result_array['Product_Total_Price'] = $result->Unit_Price;
			$result_array['Product_Discount'] = $discount;
			$result_array['Product_Total_Price_With_Discount'] = $result->Unit_Price - $discount;
			$result_array['Total_Price'] = $total_price;
			$result_array['Total_discount'] = $quantity * $discount;
			$result_array['Total_Price_With_Discount'] = $total_price - ($quantity * $discount);



			fputcsv($output, $result_array);

		}


		return fputcsv($output, $result_array);

		//return redirect('/reportbr');

	}

	public function export_pdf()
	{
		$content = "<table>";
		$content .= "<thead>";
		$content .=     "<tr >";
		$content .=         "<th>Bill_Date</th>";
		$content .=         "<th>Cust_Code</th>";
		$content .=         "<th>Activation Date</th>";
		$content .=         "<th>Deactivation Date</th>";
		$content .=         "<th>Quantity</th>";
		$content .=         "<th>Unit_Price</th>";
		$content .=         "<th>Total_Price</th>";
		$content .= "</tr>";
		$content .= "</thead>";

		$start_date =  $_POST['start_date'];
		$end_date = $_POST['end_date'];
		$line_id = $_POST['line_id'];
		$user_id = $_POST['user_id'];
		$group_id = $_POST['group_id'];
		$result_array = array();


		$query = DB::table('bod_request')
			->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
			->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
			->leftJoin('groups', 'groups.id', '=', 'lwalias.groupid')
			->leftJoin('bod_bandwidth as bdd', 'bdd.id', '=', 'bod_request.bandwidth_down')
			->leftJoin('bod_bandwidth as bdu', 'bdd.id', '=', 'bod_request.bandwidth_up')
			->leftJoin('admins', 'admins.id', '=', 'bod_request.admin_id')
			->select('bod_request.date_created',
				'bod_lines.account_no',
				'bod_request.date_created as Subscription_Activation_Date',
				'bod_request.date_end',
				'lwalias.alias',
				'bod_request.bandwidth_down',
				'bdd.bod_name',
				'bod_request.date_start',
				'bod_request.date_end as deactivation_date',
				'bdd.price as Unit_Price',
				'bod_request.burst',
				'bod_request.id',
				'bod_request.line_id',
				'bod_request.user_id',
				'lwalias.groupid')
			->where('bod_request.date_start', '>=', $start_date)
			->where('bod_request.date_end', '<=', $end_date);



		if($line_id > 0 ){
			$request_list =$query->where('bod_request.line_id','=',$line_id)->groupBy('bod_request.id')->get();;
		}elseif((!$line_id || $line_id == 0 ) && $user_id > 0 ){
			$request_list =$query->where('bod_request.user_id','=',$user_id)->groupBy('bod_request.id')->get();;
		}elseif((!$line_id || $line_id == 0 ) && (!$user_id || $user_id == 0 ) && $group_id > 0 ){
			$request_list =$query->where('lwalias.groupid','=',$group_id)->groupBy('bod_request.id')->get();;
		}else{
			$request_list =$query->groupBy('bod_request.id')->get();;
		}



		foreach($request_list  as $result){

			$discount = 0;
			$diff = abs(strtotime($end_date) - strtotime($result->date_start));

			if ($result->burst == 1) {
				$quantity = floor($diff / (60 * 60));
				$total_price = $quantity * $result->Unit_Price;
				$bill_cycle ="Hourly";
				$type="Dynamic";
			} else {
				$quantity = floor($diff / (60 * 60 * 24));
				$total_price = $quantity * $result->Unit_Price;
				$bill_cycle ="Daily";
				$type="Static";
			}


			$content .="<tr>";
			$content .= "<td>".$result->date_created."</td>";
			$content .= "<td>".$result->account_no."</td>";
			$content .= "<td>".$result->Subscription_Activation_Date."</td>";
			$content .= "<td>".$result->deactivation_date."</td>";
			$content .= "<td>".$quantity."</td>";
			$content .= "<td>".$result->Unit_Price."</td>";
			$content .= "<td>".$total_price."</td>";
			$content .="</tr>";


		}

		$content .="</table>";
		$pdf = PDF::loadHTML($content);
		return $pdf->stream();

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function reports_cron()
	{
		$unique_id = rand(1, 1000000);
		$current_date = date('Y-m-d',strtotime("-1 days"));

		$csv_folder = 'reports/';
		$filename = $current_date . "-" . $unique_id;
		$CSVFileName = $csv_folder . '/' . $filename . 'CDR.csv.xlsm';
		$FileHandle = fopen($CSVFileName, 'w') or die("can't open file");
		fclose($FileHandle);
		$output = fopen($CSVFileName, 'w');

		$start_date = $current_date.' 00:00:00';
		$end_date = $current_date.' 23:59:59';




		$result_list = DB::table('bod_request')
			->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
			->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
			->leftJoin('groups', 'groups.id', '=', 'lwalias.groupid')
			->leftJoin('bod_bandwidth as bdd', 'bdd.id', '=', 'bod_request.bandwidth_down')
			->leftJoin('bod_bandwidth as bdu', 'bdd.id', '=', 'bod_request.bandwidth_up')
			->leftJoin('admins', 'admins.id', '=', 'bod_request.admin_id')
			->leftJoin('default_bandwidth', 'default_bandwidth.id', '=', 'bod_lines.down_default')
			->select('bod_request.date_created',
						'bod_lines.account_no',
						'bod_request.date_created as Subscription_Activation_Date',
						'bod_request.date_end',
						'lwalias.alias',
						'bod_request.bandwidth_down',
						'bdd.bod_name',
						'bod_request.date_start',
						'bod_request.date_end as deactivation_date',
						'bdd.price as Unit_Price',
						'bod_request.burst',
						'bod_request.id',
						'bod_request.line_id',
						'bod_request.user_id',
						'lwalias.groupid',
						'default_bandwidth.value as down_default',
						'bod_lines.client_mac')
			->where('bod_request.date_start', '>=', $start_date)
			->groupBy('bod_request.id')
			->get();




		//->where('bod_request.date_end', '<=', $end_date)


		fputcsv($output, array('Bill_Date', 'Cust_Code', 'Subscription_Activation_Date',
			'Subscription_Deactivation_Date', 'Bill Cycle',
			'Product Code', 'Product Description', 'Quantity', 'Unit_Price',
			'Product_Total_Price', 'Product_Discount', 'Product_Total_Price_With_Discount',
			'Total_Price', 'Total_discount', 'Total_Price_With_Discount'));

		//fputcsv($output, $result_list);
		$total_array = array();


		foreach($result_list  as $result){

			$discount = 0;
			$down_default =$result->down_default;
			$client_mac =$result->client_mac;


			if ($result->burst == 1) {
				if(( ($result->date_start >= $start_date) && ($result->date_end <= $end_date ))
						|| ( ($result->date_start < $end_date) && ($result->date_end >= $end_date ))
						|| ( ($result->date_start <= $start_date) && ($result->date_end <= $end_date ))
						|| ( ($result->date_start <= $start_date) && ($result->date_end >= $end_date ))
				){
					if($result->date_start >= $start_date){
						$diff_start_date = $result->date_start;
					}else{
						$diff_start_date = $start_date;
					}

					if($result->date_end <= $end_date){
						$diff_end_date = $result->date_end;
					}else{
						$diff_end_date = $end_date;
					}

					$diff = abs(strtotime($diff_end_date) - strtotime($diff_start_date));

					$Max_down=DB::table('traffic_log')
						->where('mac_address', '=', "$client_mac")
						->where('date_start', '>=', "$start_date")
						->where('date_end', '<=', "$end_date")
						->select(DB::raw('round(MAX(traffic_log.bandwidth_down)/1024) as download'))
						->first();

					if($Max_down->download && ($down_default < $Max_down->download)){
						$price_array = DB::table('bod_bandwidth')
							->where('bod_bandwidth.max_value', '<=', $Max_down->download)
							->where('bod_bandwidth.value', '=',  $down_default)
							->where('bod_bandwidth.type', '=', 1)
							->select('bod_bandwidth.*')
							->first();

						if($price_array){
							$quantity = floor($diff / (60 * 60));
							$total_price = $quantity * $price_array->price;
							$product_code = $price_array->id;
							$product_desc =  $price_array->bod_name;

						}else{
							$quantity = 0;
							$total_price = 0;
							$product_code = 'non';
							$product_desc = 'Usage Low then Default';
						}




					}else{
						$quantity = 0;
						$total_price = 0;
						$product_code = 'non';
						$product_desc = 'Usage Low then Default';

					}

					$bill_cycle ="Hourly";
					$type="Dynamic";

					$result_array['Bill_Date'] = $result->date_created;
					$result_array['Cust_Code'] = $result->account_no;
					$result_array['Subscription_Activation_Date'] = $result->Subscription_Activation_Date;
					$result_array['Subscription_Deactivation_Date'] = $result->date_end;
					$result_array['Bill Cycle'] = $bill_cycle;
					$result_array['Product Code'] = $product_code;
					$result_array['Product Description'] = $product_desc;
					$result_array['Quantity'] = $quantity;
					$result_array['Unit_Price'] = $result->Unit_Price;
					$result_array['Product_Total_Price'] = $result->Unit_Price;
					$result_array['Product_Discount'] = $discount;
					$result_array['Product_Total_Price_With_Discount'] = $result->Unit_Price - $discount;
					$result_array['Total_Price'] = $total_price;
					$result_array['Total_discount'] = $quantity * $discount;
					$result_array['Total_Price_With_Discount'] = $total_price - ($quantity * $discount);


					fputcsv($output, $result_array);
					
				}



			} else {
				if(( ($result->date_start >= $start_date) && ($result->date_end <= $end_date ))
					|| ( ($result->date_start < $end_date) && ($result->date_end >= $end_date ))
					|| ( ($result->date_start <= $start_date) && ($result->date_end <= $end_date ))
					|| ( ($result->date_start <= $start_date) && ($result->date_end >= $end_date ))
				){
					$diff = abs(strtotime($start_date) - strtotime($end_date));
					$quantity = 1;
					$total_price = $quantity * $result->Unit_Price;
					$bill_cycle ="Daily";
					$type="Static";
					$product_code = $result->bandwidth_down;
					$product_desc = $result->bod_name;

					$result_array['Bill_Date'] = $result->date_created;
					$result_array['Cust_Code'] = $result->account_no;
					$result_array['Subscription_Activation_Date'] = $result->Subscription_Activation_Date;
					$result_array['Subscription_Deactivation_Date'] = $result->date_end;
					$result_array['Bill Cycle'] = $bill_cycle;
					$result_array['Product Code'] = $product_code;
					$result_array['Product Description'] = $product_desc;
					$result_array['Quantity'] = $quantity;
					$result_array['Unit_Price'] = $result->Unit_Price;
					$result_array['Product_Total_Price'] = $result->Unit_Price;
					$result_array['Product_Discount'] = $discount;
					$result_array['Product_Total_Price_With_Discount'] = $result->Unit_Price - $discount;
					$result_array['Total_Price'] = $total_price;
					$result_array['Total_discount'] = $quantity * $discount;
					$result_array['Total_Price_With_Discount'] = $total_price - ($quantity * $discount);

					fputcsv($output, $result_array);
				}
			}


		}

		return redirect('list_cdr')->with('flash_success', 'New CDR Report Generated successfully!.');






	}

	public function list_cdr(){
		$base_path =Config::get('app.public_path').'/reports';

		$list = File::allFiles($base_path);


		return view('reports.list_cdr',
			[
				'cdr_list' => $list,
			]);
	}


	public function destroy_cdr($url){
		$file_path =Config::get('app.public_path').'/reports/'.$url;

		$success = File::delete($file_path);

		if($success == TRUE){
			return redirect('list_cdr')->with('flash_success', 'CDR File deleted successfully');
		}else{
			return redirect('list_cdr')->with('flash_message', 'CDR File Not deleted,try again..');
		}




	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
