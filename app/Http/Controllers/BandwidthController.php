<?php namespace App\Http\Controllers;

use App\Bod_request;
use Flash;
use App\Action_log;
use DB;
use Session;
use DateTime;
Use Validator;
use Auth;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class BandwidthController extends Controller {


	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Ajax Return Function for list Gateways in select drop down.
	 *
	 * @return Response
	 */
	public function load_gateways()
	{
		if(Auth::user()->access == 2 ){
			$gateway_list = DB::table('lwalias')->select('id', 'alias')->where('groupid', '=', $_POST['group_id'])->get();
		}else{
			$gateway_list = DB::table('lwalias')->select('id', 'alias')->where('groupid', '=', Auth::user()->groupid )->get();
		}


		if($gateway_list){
			$gateway_array[0] ="[Select a Gateway]";
			foreach($gateway_list  as $gateway){
				$gateway_array[$gateway->id]= $gateway->alias;

			}
		}else{
			$gateway_array[0] ="[No Gateway for this Group]";
		}


		return view('bandwidth.load_gateways', ['gateway_list' => $gateway_array]);

	}


	/**
	 * Ajax Return Function for list Lines in select drop down.
	 *
	 * @return Response
	 */
	public function load_lines()
	{

		$lines_list = DB::table('bod_lines')
			->join('interfaces','interfaces.line','=','bod_lines.interface')
			->select('bod_lines.id', 'interfaces.description')
			->where('bod_lines.alias_id', '=', $_POST['user_id'])
			->get();


		if($lines_list){
			$line_array[0] ="[Select a line]";
			foreach($lines_list  as $line){
				/*if($line->description){*/
					$line_array[$line->id]= $line->description;
				/*}*/
			}
		}else{
			$line_array[0] ="[Lines Not configured]";
		}


		return view('bandwidth.load_lines', ['line_list' => $line_array]);

	}

	/**
	 * Ajax Return Function for list Bandwidth(Less then gateways max amount) types in select drop down.
	 *
	 * @return Response
	 */
	public function load_bandwidth()
	{

		$down_content ='<select name="bandwidth_down"  id="bandwidth_down" class="form-control">';
		$up_content ='<select name="bandwidth_up"  id="bandwidth_up" class="form-control">';
		$down_content .='<option value="0" >[Select]</option>';
		$up_content .='<option value="0" >[Select]</option>';
		if($_POST['line_id']){
			$down_list = DB::table('bod_bandwidth')
							->leftJoin('bod_lines','bod_bandwidth.value','<=','bod_lines.down_max')
							->select('bod_bandwidth.id', 'bod_bandwidth.bod_name')
							->where('bod_lines.id', '=', $_POST['line_id'])
							->where('bod_bandwidth.type', '=', 0)
							->get();
			foreach($down_list  as $bandwidth){
				$down_content .='<option value="'.$bandwidth->id.'" >'.$bandwidth->bod_name.'</option>';

			}

			$up_list = DB::table('bod_bandwidth')
							->leftJoin('bod_lines','bod_bandwidth.value','<=','bod_lines.up_max')
							->select('bod_bandwidth.id', 'bod_bandwidth.bod_name')
							->where('bod_lines.id', '=', $_POST['line_id'])
							->where('bod_bandwidth.type', '=', 0)
							->get();
			foreach($up_list  as $bandwidth){
				$up_content .='<option value="'.$bandwidth->id.'" >'.$bandwidth->bod_name.'</option>';

			}

		}else{
			$down_content .='<option value="" >[Bandwith Not Configured]</option>';
			$up_content .='<option value="" >[Bandwith Not Configured]</option>';
		}

		$down_content .='</select>';
		$up_content .='</select>';

		$return_value = array('down' => '', 'up' => '');

		$return_value['down'] =$down_content;
		$return_value['up'] =$up_content;

		return json_encode($return_value);

	}




	/**
	 * Display a listing of Static Bandwidth Request.
	 *
	 * @return Response
	 */
	public function staticreq($id="")
	{
		if($id == "" || $id == 0){
			if(Auth::user()->access == 2 ){
				$request_list = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 0)
					->get();


			}elseif(Auth::user()->access == 1 ){
				$request_list = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 0)
					->where('lwalias.groupid', '=', Auth::user()->groupid)
					->get();

			}else{
				$request_list = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 0)
					->where('bod_request.admin_id', '=', Auth::user()->id)
					->get();

			}

			return view('bandwidth.static', ['request_list' => $request_list,'url_id' => $id]);

		}else{
			$dt = new DateTime();
			$current_dt = $dt->format('Y-m-d');





			if(Auth::user()->access == 2 ){
				$request_query = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 0);
				/*if($id == -2 ){
					$request_query->where('bod_request.date_start', '>', $current_dt);
				}elseif($id == -3 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.date_start', '<=', $current_dt)->where('bod_request.status', '=', 0);
				}elseif($id == -1 ){
					$request_query->where('bod_request.date_end', '<', $current_dt);
				}elseif($id == 1 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.date_start', '<=', $current_dt)->where('bod_request.status', '=', 1);
				}else{
					$request_query->where('bod_request.id', '>', 0);
				}*/
				if($id == -2 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.status', '=', 0);
				}elseif($id == -1 ){
					$request_query->where('bod_request.date_end', '<', $current_dt);
				}elseif($id == 1 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.date_start', '<=', $current_dt)->where('bod_request.status', '=', 1);
				}else{
					$request_query->where('bod_request.id', '>', 0);
				}

				$request_list =$request_query->get();


			}elseif(Auth::user()->access == 1 ){
				$request_query = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 0)
					->where('lwalias.groupid', '=', Auth::user()->groupid);
				if($id == -2 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.status', '=', 0);
				}elseif($id == -1 ){
					$request_query->where('bod_request.date_end', '<', $current_dt);
				}elseif($id == 1 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.date_start', '<=', $current_dt)->where('bod_request.status', '=', 1);
				}else{
					$request_query->where('bod_request.id', '>', 0);
				}

				$request_list =$request_query->get();

			}else{
				$request_query = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 0)
					->where('lwalias.id', '=', Auth::user()->aliasid);
				if($id == -2 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.status', '=', 0);
				}elseif($id == -1 ){
					$request_query->where('bod_request.date_end', '<', $current_dt);
				}elseif($id == 1 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.date_start', '<=', $current_dt)->where('bod_request.status', '=', 1);
				}else{
					$request_query->where('bod_request.id', '>', 0);
				}

				$request_list =$request_query->get();

			}

			return view('bandwidth.static', ['request_list' => $request_list,'url_id' => $id]);

		}


	}


	/**
	 * Show  the form for editing the specified/New Static Bandwidth request.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function newstaticreq($id=null)
	{
		$group_list = DB::table('groups')->select('id', 'name')->get();
		$group_array[0] ="[Select a Group]";
		foreach($group_list  as $group){
			$group_array[$group->id]= $group->name;

		}
		if(Auth::user()->access == 2 ){
			$gateway_list = DB::table('lwalias')->select('id', 'alias')->get();
		}else{
			$gateway_list = DB::table('lwalias')->select('id', 'alias')->where('groupid', '=', Auth::user()->groupid )->get();
		}
		$gateway_array[0] ="[Select a Gateway]";
		foreach($gateway_list  as $gateway){
			$gateway_array[$gateway->id]= $gateway->alias;

		}

		$line_list = DB::table('bod_lines')->select('id', 'interface')->get();
		$line_array[0] ="[Select a Line]";
		foreach($line_list  as $line){
			$line_array[$line->id]= $line->interface;

		}



		$bandwidth_list = DB::table('bod_bandwidth')->select('id', 'bod_name')->where('type', '=', 0)->get();
		$bandwidth_array[0] ="[Select a Bandwidth]";
		foreach($bandwidth_list  as $bandwidth){
			$bandwidth_array[$bandwidth->id]= $bandwidth->bod_name;

		}

		if($id){
			$bandwidth_request = DB::table('bod_request')
									->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
									->select('bod_request.*' , 'lwalias.groupid')
									->where('bod_request.id', '=', $id)
									->first();
			return view('bandwidth.edit_static',
				['bandwidth_request' => $bandwidth_request,
					'group_array' => $group_array,
					'bandwidth_array' => $bandwidth_array,
					'gateway_array' => $gateway_array,
					'line_array' => $line_array,
				]);

		}else{
			return view('bandwidth.new_static', ['group_array' => $group_array,'bandwidth_array' => $bandwidth_array,'gateway_array' => $gateway_array]);
		}
	}


	/**
	 * Store a newly created Static Request in storage.
	 *
	 * @return Response
	 */
	public function storestaticreq(Request $request)
	{
		$v = Validator::make($request->all(), [
			'user_id' => 'required|integer',
			'line_id' => 'required|integer',
			'bandwidth_down' => 'required|integer',
			'bandwidth_up' => 'required|integer',
			/*'date_start' => 'required|date_format:Y-m-d',*/
			'date_start' => 'required',
			'date_end' => 'required',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{
			$date_start = $request->input('date_start')." 00:00:00";
			$date_end = $request->input('date_end')." 23:59:59";
			$count_one = DB::table('bod_request')
				->whereIn('id', function($query) use ($date_start)
				{
					$query->select('id')
						->from('bod_request')
						->where('date_end', '>' , $date_start);
				})
				->whereIn('id', function($query) use ($date_end)
				{
					$query->select('id')
						->from('bod_request')
						->where('date_start', '<' , $date_end);
				})
				->where('user_id', '=', $request->input('user_id'))
				->where('line_id', '=', $request->input('line_id'))
				->count();

			$count_two = DB::table('bod_request')
				->where(function($query) use ($date_end,$date_start)
				{
					$query->where('date_end', '=', $date_end)
						->where('date_start', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->where('date_end', '=', $date_end)
						->where('date_end', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->where('date_start', '=', $date_end)
						->where('date_start', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->whereBetween('date_end', [ $date_end,new DateTime('today') ]);
				})

				->where('line_id', '=', $request->input('line_id'))
				->count();

			//->where('id', '!=', $request->input('user_id'))

			/*DB::table('users')
				->whereIn('id', $request->input('date_start'))
				->get();*/


			if ($count_one > 0  || $count_two > 0 ) {
				return redirect('staticreq')->with('flash_message', 'Dear Customer, there is currently a request booked for that date.');

			}else{

				$line_row = DB::table('bod_lines')->where('id','=' ,$request->input('line_id'))->first();

				$bd_request = new Bod_request();
				$bd_request->user_id = $request->input('user_id');
				$bd_request->line_id = $request->input('line_id');
				$bd_request->admin_id = Auth::user()->id;
				$bd_request->bandwidth_down = $request->input('bandwidth_down');
				$bd_request->bandwidth_up = $request->input('bandwidth_up');
				$bd_request->date_start = $request->input('date_start')." 00:00:00";
				$bd_request->date_end = $request->input('date_end')." 23:59:59";
				$bd_request->device_mac = $line_row->client_mac;
				$bd_request->burst = 0;
				$bd_request->save();

				/* action log insertion */
				$action_log = new Action_log();
				$action_log->userid = Auth::user()->id;
				$action_log->username = Auth::user()->name;
				$action_log->action = ' Static Bandwidth Request Successfully Stored';
				$action_log->save();
				/* action log insertion */


				return redirect('staticreq')->with('flash_success', 'Record Inserted successfully!.');

			}



		}

	}


	/**
	 * Store Updated records Static request in storage.
	 *
	 * @param  array  $request
	 * @return Static Request list view
	 */
	public function update_staticreq(Request $request)
	{
		$v = Validator::make($request->all(), [
			'user_id' => 'required|integer',
			'line_id' => 'required|integer',
			'bandwidth_down' => 'required|integer',
			'bandwidth_up' => 'required|integer',
			/*'date_start' => 'required|date_format:Y-m-d',*/
			'date_start' => 'required',
			'date_end' => 'required',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{
			$date_start = $request->input('date_start')." 00:00:00";
			$date_end = $request->input('date_end')." 23:59:59";
			$count_one = DB::table('bod_request')
				->whereIn('id', function($query) use ($date_start)
				{
					$query->select('id')
						->from('bod_request')
						->where('date_end', '>' , $date_start);
				})
				->whereIn('id', function($query) use ($date_end)
				{
					$query->select('id')
						->from('bod_request')
						->where('date_start', '<' , $date_end);
				})
				->where('user_id', '=', $request->input('user_id'))
				->where('line_id', '=', $request->input('line_id'))
				->where('id', '!=', $request->input('id'))
				->count();

			$count_two = DB::table('bod_request')
				->where(function($query) use ($date_end,$date_start)
				{
					$query->where('date_end', '=', $date_end)
						->where('date_start', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->where('date_end', '=', $date_end)
						->where('date_end', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->where('date_start', '=', $date_end)
						->where('date_start', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->whereBetween('date_end', [ $date_end,new DateTime('today') ]);
				})
				->where('id', '!=', $request->input('id'))
				->where('line_id', '=', $request->input('line_id'))
				->count();

			//

			/*DB::table('users')
				->whereIn('id', $request->input('date_start'))
				->get();*/


			if ($count_one > 0  || $count_two > 0 ) {
				return redirect('staticreq')->with('flash_message', 'Dear Customer, there is currently a request booked for that date.');

			}else{
				$line_row = DB::table('bod_lines')->where('id','=' ,$request->input('line_id'))->first();
				$bd_request = Bod_request::find($request->input('id'));
				$bd_request->user_id = $request->input('user_id');
				$bd_request->line_id = $request->input('line_id');
				$bd_request->admin_id = Auth::user()->id;
				$bd_request->bandwidth_down = $request->input('bandwidth_down');
				$bd_request->bandwidth_up = $request->input('bandwidth_up');
				$bd_request->date_start = $request->input('date_start')." 00:00:00";
				$bd_request->date_end = $request->input('date_end')." 23:59:59";
				$bd_request->device_mac = $line_row->client_mac;
				$bd_request->burst = 0;
				$bd_request->save();

				/* action log insertion */
				$action_log = new Action_log();
				$action_log->userid = Auth::user()->id;
				$action_log->username = Auth::user()->name;
				$action_log->action = ' Static Bandwidth Request updated Successfully';
				$action_log->save();
				/* action log insertion */


				return redirect('staticreq')->with('flash_success', 'Record Updated successfully!.');



			}


		}

	}


	/**
	 * Remove the specified Static Bandwidth request from storage.
	 *
	 * @param  int  $id
	 * @return Static request listing page.
	 */
	public function destroy_staticreq($id)
	{
		$affectedRows = Bod_request::where('id', '=', $id)->delete();
		if($affectedRows){

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Static Bandwidth Request Delected Successfully';
			$action_log->save();
			/* action log insertion */

			return redirect('staticreq')->with('flash_success', 'Your Record Deleted successfully!.');
		}else{
			return redirect('staticreq')->with('flash_message', 'Record not deleted,try again...');
		}

	}



	/**
	 * Display a listing of Dynamic Bandwidth Request.
	 *
	 * @return Response
	 */
	public function dynamicreq($id="")
	{
		if($id == "" || $id == 0){
			if(Auth::user()->access == 2 ){
				$request_list = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 1)
					->get();


			}elseif(Auth::user()->access == 1 ){
				$request_list = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 1)
					->where('lwalias.groupid', '=', Auth::user()->groupid)
					->get();

			}else{
				$request_list = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 1)
					->where('bod_request.admin_id', '=', Auth::user()->id)
					->get();

			}

			return view('bandwidth.dynamic', ['request_list' => $request_list,'url_id' => $id]);

		}else{
			$dt = new DateTime();
			$current_dt = $dt->format('Y-m-d H:i:s');

			if(Auth::user()->access == 2 ){
				$request_query = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 1);
				if($id == -2 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.status', '=', 0);
				}elseif($id == -1 ){
					$request_query->where('bod_request.date_end', '<', $current_dt);
				}elseif($id == 1 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.date_start', '<=', $current_dt)->where('bod_request.status', '=', 1);
				}else{
					$request_query->where('bod_request.id', '>', 0);
				}

				$request_list =$request_query->get();


			}elseif(Auth::user()->access == 1 ){
				$request_query = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 1)
					->where('lwalias.groupid', '=', Auth::user()->groupid);
				if($id == -2 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.status', '=', 0);
				}elseif($id == -1 ){
					$request_query->where('bod_request.date_end', '<', $current_dt);
				}elseif($id == 1 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.date_start', '<=', $current_dt)->where('bod_request.status', '=', 1);
				}else{
					$request_query->where('bod_request.id', '>', 0);
				}

				$request_list =$request_query->get();

			}else{
				$request_query = DB::table('bod_request')
					->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
					->leftJoin('bod_lines', 'bod_lines.id', '=', 'bod_request.line_id')
					->leftJoin('bod_bandwidth as downband', 'downband.id', '=', 'bod_request.bandwidth_down')
					->leftJoin('bod_bandwidth as upband', 'upband.id', '=', 'bod_request.bandwidth_up')
					->select('bod_request.*' , 'lwalias.alias', 'bod_lines.interface', 'downband.bod_name as down_name', 'upband.bod_name as up_name')
					->where('bod_request.burst', '=', 1)
					->where('lwalias.id', '=', Auth::user()->aliasid);
				if($id == -2 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.status', '=', 0);
				}elseif($id == -1 ){
					$request_query->where('bod_request.date_end', '<', $current_dt);
				}elseif($id == 1 ){
					$request_query->where('bod_request.date_end', '>=', $current_dt)->where('bod_request.date_start', '<=', $current_dt)->where('bod_request.status', '=', 1);
				}else{
					$request_query->where('bod_request.id', '>', 0);
				}

				$request_list =$request_query->get();

			}

			return view('bandwidth.dynamic', ['request_list' => $request_list,'url_id' => $id]);

		}


	}

	/**
	 * Show  the form for editing the specified/New Dynamic Bandwidth request.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function newdynamicreq($id=null)
	{

		$group_list = DB::table('groups')->select('id', 'name')->get();
		$group_array[0] ="[Select a Group]";
		foreach($group_list  as $group){
			$group_array[$group->id]= $group->name;

		}
		if(Auth::user()->access == 2 ){
			$gateway_list = DB::table('lwalias')->select('id', 'alias')->get();
		}else{
			$gateway_list = DB::table('lwalias')->select('id', 'alias')->where('groupid', '=', Auth::user()->groupid )->get();
		}
		$gateway_array[0] ="[Select a Gateway]";
		foreach($gateway_list  as $gateway){
			$gateway_array[$gateway->id]= $gateway->alias;

		}

		$line_list = DB::table('bod_lines')->select('id', 'interface')->get();
		$line_array[0] ="[Select a Line]";
		foreach($line_list  as $line){
			$line_array[$line->id]= $line->interface;

		}

		$bandwidth_list = DB::table('bod_bandwidth')->select('id', 'bod_name')->where('type', '=', 1)->get();
		$bandwidth_array[0] ="[Select a Bandwidth]";
		foreach($bandwidth_list  as $bandwidth){
			$bandwidth_array[$bandwidth->id]= $bandwidth->bod_name;

		}

		if($id){
			$bandwidth_request = DB::table('bod_request')
								->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
								->select('bod_request.*' , 'lwalias.groupid')
								->where('bod_request.id', '=', $id)
								->first();
			return view('bandwidth.edit_dynamic',
				['bandwidth_request' => $bandwidth_request,
					'group_array' => $group_array,
					'bandwidth_array' => $bandwidth_array,
					'gateway_array' => $gateway_array,
					'line_array' => $line_array,
				]);

		}else{
			return view('bandwidth.new_dynamic', ['group_array' => $group_array,'bandwidth_array' => $bandwidth_array ,'gateway_array' => $gateway_array]);
		}


	}

	/**
	 * Store a newly created Dynamic Request in storage.
	 *
	 * @return Response
	 */
	public function storedynamicreq(Request $request)
	{
		$v = Validator::make($request->all(), [
			'user_id' => 'required|integer',
			'line_id' => 'required|integer',
			'date_from' => 'required',
			'date_to' => 'required',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{
			$date_start = $request->input('date_from');
			$date_end = $request->input('date_to');
			$count_one = DB::table('bod_request')
				->whereIn('id', function($query) use ($date_start)
				{
					$query->select('id')
						->from('bod_request')
						->where('date_end', '>' , $date_start);
				})
				->whereIn('id', function($query) use ($date_end)
				{
					$query->select('id')
						->from('bod_request')
						->where('date_start', '<' , $date_end);
				})
				->where('user_id', '=', $request->input('user_id'))
				->where('line_id', '=', $request->input('line_id'))
				->count();

			$count_two = DB::table('bod_request')
				->where(function($query) use ($date_end,$date_start)
				{
					$query->where('date_end', '=', $date_end)
						->where('date_start', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->where('date_end', '=', $date_end)
						->where('date_end', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->where('date_start', '=', $date_end)
						->where('date_start', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->whereBetween('date_end', [ $date_end,new DateTime('today') ]);
				})
				->where('line_id', '=', $request->input('line_id'))
				->count();

			//

			/*DB::table('users')
				->whereIn('id', $request->input('date_start'))
				->get();*/


			if ($count_one > 0  || $count_two > 0 ) {
				return redirect('dynamicreq')->with('flash_message', 'Dear Customer, there is currently a request booked for that date.');

			}else{
				$line_row = DB::table('bod_lines')->where('id','=' ,$request->input('line_id'))->first();

				dd($request->input('date_from'));

				$bd_request = new Bod_request();
				$bd_request->user_id = $request->input('user_id');
				$bd_request->line_id = $request->input('line_id');
				$bd_request->admin_id = Auth::user()->id;
				$bd_request->date_start = $request->input('date_from');
				$bd_request->date_end = $request->input('date_to');
				$bd_request->device_mac = $line_row->client_mac;
				$bd_request->burst = 1;
				$bd_request->save();

				/* action log insertion */
				$action_log = new Action_log();
				$action_log->userid = Auth::user()->id;
				$action_log->username = Auth::user()->name;
				$action_log->action = ' Dynamic Bandwidth Request Successfully Recorded';
				$action_log->save();
				/* action log insertion */

				return redirect('dynamicreq')->with('flash_success', 'Record Inserted successfully!.');

			}


		}
	}


	/**
	 * Store Updated records Dynamic request in storage.
	 *
	 * @param  array  $request
	 * @return Dynamic Request list view
	 */
	public function update_dynamicreq(Request $request)
	{
		$v = Validator::make($request->all(), [
			'user_id' => 'required|integer',
			'line_id' => 'required|integer',
			'date_from' => 'required',
			'date_to' => 'required',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{
			$date_start = $request->input('date_from');
			$date_end = $request->input('date_to');
			$count_one = DB::table('bod_request')
				->whereIn('id', function($query) use ($date_start)
				{
					$query->select('id')
						->from('bod_request')
						->where('date_end', '>' , $date_start);
				})
				->whereIn('id', function($query) use ($date_end)
				{
					$query->select('id')
						->from('bod_request')
						->where('date_start', '<' , $date_end);
				})
				->where('user_id', '=', $request->input('user_id'))
				->where('line_id', '=', $request->input('line_id'))
				->where('id', '!=', $request->input('id'))
				->count();

			$count_two = DB::table('bod_request')
				->where(function($query) use ($date_end,$date_start)
				{
					$query->where('date_end', '=', $date_end)
						->where('date_start', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->where('date_end', '=', $date_end)
						->where('date_end', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->where('date_start', '=', $date_end)
						->where('date_start', '=', $date_start);
				})
				->orWhere(function($query) use ($date_end,$date_start)
				{
					$query->whereBetween('date_end', [ $date_end,new DateTime('today') ]);
				})
				->where('id', '!=', $request->input('id'))
				->where('line_id', '=', $request->input('line_id'))
				->count();

			//

			/*DB::table('users')
				->whereIn('id', $request->input('date_start'))
				->get();*/


			if ($count_one > 0  || $count_two > 0 ) {
				return redirect('dynamicreq')->with('flash_message', 'Dear Customer, there is currently a request booked for that date.');

			}else{
				$line_row = DB::table('bod_lines')->where('id','=' ,$request->input('line_id'))->first();

				$bd_request = Bod_request::find($request->input('id'));
				$bd_request->user_id = $request->input('user_id');
				$bd_request->line_id = $request->input('line_id');
				$bd_request->admin_id = Auth::user()->id;
				$bd_request->date_start = $request->input('date_from');
				$bd_request->date_end = $request->input('date_to');
				$bd_request->device_mac = $line_row->client_mac;
				$bd_request->burst = 1;
				$bd_request->save();

				/* action log insertion */
				$action_log = new Action_log();
				$action_log->userid = Auth::user()->id;
				$action_log->username = Auth::user()->name;
				$action_log->action = ' Dynamic Bandwidth Request Successfully Updated';
				$action_log->save();
				/* action log insertion */


				return redirect('dynamicreq')->with('flash_success', 'Record Updated successfully!.');

			}

		}
	}

	/**
	 * Remove the specified Dynamic Bandwidth request from storage.
	 *
	 * @param  int  $id
	 * @return Dynamic request listing page.
	 */
	public function destroy_dynamicreq($id)
	{
		$affectedRows = Bod_request::where('id', '=', $id)->delete();
		if($affectedRows){

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Dynamic Bandwidth Request Delected Successfully';
			$action_log->save();
			/* action log insertion */

			return redirect('dynamicreq')->with('flash_success', 'Your Record Deleted successfully!.');
		}else{
			return redirect('dynamicreq')->with('flash_message', 'Record not deleted,Try again....');
		}

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
