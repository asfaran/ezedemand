<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
Route::get('dashboard', 'DashboardController@index');

Route::get('administrator', 'ConfigureController@administrator');
Route::get('new_admin', 'ConfigureController@new_admin');
Route::post('regadmin', 'ConfigureController@regadmin');
Route::get('destroy_admin/{id}', [
	'as' => 'profile', 'uses' => 'ConfigureController@destroy_admin'
]);
Route::get('new_admin/{id}', [
	'as' => 'profile', 'uses' => 'ConfigureController@new_admin'
]);
Route::post('update_admin', 'ConfigureController@update_admin');



Route::get('groups', 'ConfigureController@groups');
Route::get('newgroup', 'ConfigureController@newgroup');
Route::post('storegroup', 'ConfigureController@storegroup');
Route::get('destroy_group/{id}', [
	'as' => 'profile', 'uses' => 'ConfigureController@destroy_group'
]);
Route::get('newgroup/{id}', [
	'as' => 'profile', 'uses' => 'ConfigureController@newgroup'
]);
Route::post('update_group', 'ConfigureController@update_group');


Route::get('rights', 'ConfigureController@rights');
Route::post('update_rights', 'ConfigureController@update_rights');
Route::post('load_rights', 'ConfigureController@load_rights');


Route::get('statictype', 'ConfigureController@statictype');
Route::get('newstatic', 'ConfigureController@newstatic');
Route::post('storestatic', 'ConfigureController@storestatic');
Route::get('destroy_static/{id}', [
	'as' => 'profile', 'uses' => 'ConfigureController@destroy_static'
]);
Route::get('newstatic/{id}', [
	'as' => 'profile', 'uses' => 'ConfigureController@newstatic'
]);
Route::post('update_static', 'ConfigureController@update_static');

Route::get('dynamictype', 'ConfigureController@dynamictype');
Route::get('newdynamic', 'ConfigureController@newdynamic');
Route::post('storedynamic', 'ConfigureController@storedynamic');
Route::get('destroy_dynamic/{id}', [
	'as' => 'profile', 'uses' => 'ConfigureController@destroy_dynamic'
]);
Route::get('newdynamic/{id}', [
	'as' => 'profile', 'uses' => 'ConfigureController@newdynamic'
]);
Route::post('update_dynamic', 'ConfigureController@update_dynamic');
Route::get('default_bandwidth', 'ConfigureController@default_bandwidth');
Route::get('new_bandwidth', 'ConfigureController@new_bandwidth');
Route::post('destroy_bandwidth', 'ConfigureController@destroy_bandwidth');
Route::post('update_bandwidth', 'ConfigureController@update_bandwidth');
Route::post('store_bandwidth', 'ConfigureController@store_bandwidth');
Route::get('new_bandwidth/{id}', [
	'as' => 'profile', 'uses' => 'ConfigureController@new_bandwidth'
]);

Route::get('interfaces', 'ConfigureController@interfaces');
Route::post('update_interface', 'ConfigureController@update_interface');

Route::get('staticreq', 'BandwidthController@staticreq');
Route::get('newstaticreq', 'BandwidthController@newstaticreq');
Route::post('storestaticreq', 'BandwidthController@storestaticreq');
Route::post('load_gateways', 'BandwidthController@load_gateways');
Route::post('load_lines', 'BandwidthController@load_lines');
Route::get('destroy_staticreq/{id}', [
	'as' => 'profile', 'uses' => 'BandwidthController@destroy_staticreq'
]);
Route::get('newstaticreq/{id}', [
	'as' => 'profile', 'uses' => 'BandwidthController@newstaticreq'
]);
Route::post('update_staticreq', 'BandwidthController@update_staticreq');
Route::get('staticreq/{id}', [
	'as' => 'profile', 'uses' => 'BandwidthController@staticreq'
]);



Route::post('load_bandwidth', 'BandwidthController@load_bandwidth');
Route::get('dynamicreq', 'BandwidthController@dynamicreq');
Route::get('newdynamicreq', 'BandwidthController@newdynamicreq');
Route::post('storedynamicreq', 'BandwidthController@storedynamicreq');
Route::get('destroy_dynamicreq/{id}', [
	'as' => 'profile', 'uses' => 'BandwidthController@destroy_dynamicreq'
]);
Route::get('newdynamicreq/{id}', [
	'as' => 'profile', 'uses' => 'BandwidthController@newdynamicreq'
]);
Route::post('update_dynamicreq', 'BandwidthController@update_dynamicreq');
Route::get('dynamicreq/{id}', [
	'as' => 'profile', 'uses' => 'BandwidthController@dynamicreq'
]);


/*  entered to menu db table */
Route::get('list_cdr', 'ReportController@list_cdr');
Route::get('destroy_cdr/{id}', [
	'as' => 'destroy_cdr', 'uses' => 'ReportController@destroy_cdr'
]);
Route::get('reportbr', 'ReportController@reportbr');
Route::get('bdmonitor', 'ReportController@bdmonitor');
Route::post('generate_bdmonitor', 'ReportController@generate_bdmonitor');
Route::post('generate_reportbr', 'ReportController@generate_reportbr');
Route::post('load_period', 'ReportController@load_period');
Route::post('export_csv', 'ReportController@export_csv');
Route::post('export_pdf', 'ReportController@export_pdf');
Route::get('reports_cron', 'ReportController@reports_cron');

Route::post('lines_maxdown', 'GatewayController@lines_maxdown');
Route::post('lines_maxup', 'GatewayController@lines_maxup');
Route::get('gateway', 'GatewayController@index');
Route::get('edit_gateway/{id}', [
	'as' => 'profile', 'uses' => 'GatewayController@edit_gateway'
]);
Route::get('destroy_gateway/{id}', [
	'as' => 'profile', 'uses' => 'GatewayController@destroy_gateway'
]);
Route::post('update_gateway', 'GatewayController@update_gateway');
Route::get('gateway/{id}', [
	'as' => 'profile', 'uses' => 'GatewayController@index'
]);
Route::post('load_linelist', 'GatewayController@load_linelist');
Route::post('loadcities', 'GatewayController@loadcities');









