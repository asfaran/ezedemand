@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Configuration
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Configuration</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="">Administrator/User</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('new_admin')}}">Add New</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add New Administrator/User
                    </div>
                    <div class="tools">
                        <a class="collapse" href=""></a>
                        <a class="reload" href=""></a>
                    </div>
                </div>

                <div class="portlet-body form">
                    {!! Form::open(array('url'=>'regadmin','role'=>'form', 'class'=>'form-horizontal')) !!}

                    <div class="form-body">
                        <div class="form-group">
                            {!! Form::label('name','User Name',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::text('name','',array('id'=>'','class'=>'form-control','placeholder'=>'Enter a New User Name')) !!}
                                @if ($errors->has('name'))
                                    <span class="alert-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        {{--<div class="form-group">
                            {!! Form::label('etisalatid','Etisalat Client ID',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::text('account_no','',array('id'=>'clientid','class'=>'form-control','placeholder'=>'Etisalat Client ID')) !!}
                                @if ($errors->has('account_no'))
                                    <span class="alert-danger">{{ $errors->first('account_no') }}</span>
                                @endif
                            </div>
                        </div>--}}
                        <div class="form-group">
                            {!! Form::label('access','User Level',array('id'=>'','class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                @if (Auth::user()->access == 2)
                                    {!! Form::select('access',array(''=>'[Select a User Level]', 1 =>'Administrator',0 =>'User'),'enabled',array('id'=>'user_access','class'=>'form-control')) !!}
                                @else
                                    {!! Form::select('access',array(''=>'[Select a User Level]',0 =>'User'),'enabled',array('id'=>'user_access','class'=>'form-control')) !!}
                                @endif

                                @if ($errors->has('access'))
                                    <span class="alert-danger">{{ $errors->first('access') }}</span>
                                @endif
                            </div>
                        </div>
                        @if(Auth::user()->access == 2)
                        <div class="form-group" id="admin_group_area" >
                            {!! Form::label('groupid','Client Group',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::select('groupid',$group_array,'',array('id'=>'groupid','class'=>'form-control')) !!}
                                @if ($errors->has('groupid'))
                                    <span class="alert-danger">{{ $errors->first('groupid') }}</span>
                                @endif
                                @if ($errors->has('user_id'))
                                    <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group" id="admin_gateway_area"  style="display: none">
                            {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4" id="gateway_result">
                                {!! Form::select('user_id', $gateway_array , null, ['id' => 'user_id', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        @endif
                        @if(Auth::user()->access == 1)
                            <div class="form-group" >
                                {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-4" id="gateway_result">
                                    {!! Form::select('user_id', $gateway_array , null, ['id' => 'user_id', 'class' => 'form-control']) !!}
                                    @if ($errors->has('user_id'))
                                        <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                                    @endif
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            {!! Form::label('mail','Email Address',array('id'=>'','class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    {!! Form::text('email','',array('id'=>'','class'=>'form-control','placeholder'=>'Email Address')) !!}
                                    @if ($errors->has('email'))
                                        <span class="alert-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password','Password',array('id'=>'','class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::password('password',array('id'=>'password', 'class'=>'form-control','placeholder'=>'Password')) !!}
                                    @if ($errors->has('password'))
                                        <span class="alert-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password_confirmation','Confirm Password',array('id'=>'','class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::password('password_confirmation',array('id'=>'password_confirmation','class'=>'form-control','placeholder'=>'Confirm Password')) !!}
                                    @if ($errors->has('password_confirmation'))
                                        <span class="alert-danger">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('contact','Contact Number',array('id'=>'','class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::text('contact','',array('id'=>'contact','class'=>'form-control','placeholder'=>'+971XXXXXX..')) !!}
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Save',array('class'=>'btn green')) !!}
                            <a href="{{URL::to('/administrator')}}"><button class="btn default" type="button">Cancel</button></a>
                        </div>
                    </div>
                    {!! Form::close() !!}


                </div>
            </div>


        </div>
    </div>
@endsection