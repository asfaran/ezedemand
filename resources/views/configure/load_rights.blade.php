<table class="table table-striped table-bordered table-hover " id="admin_rights">
    <thead>
    <tr>
        <th style="text-align:left">Module</th>
        <th style="text-align:left">Access</th>
    </tr>
    </thead>
    <tbody>



        @foreach($menu_list as $menu)
        <?php  $parent_list[] = $menu->parentid; ?>
        @if($menu->parentid == false)
            <tr>
                <td class="tbox">{!! Form::label($menu->text,$menu->text,array('class'=>'bold control-label')) !!}</td>
                <td class="tbox"></td>
            </tr>
                        @foreach($menu_list as $submenu)

                            @if($submenu->parentid == $menu->id)
                                <tr>
                                    <td class="tbox">
                                        <span class="halflings halflings-menu-right"></span>
                                        <span style="margin-left:15px;">{!! Form::label( $submenu->text,$submenu->text ,array('class'=>'control-label')) !!}</span>
                                    </td>
                                    <td class="tbox">
                                        <input id="{{  $submenu->id }}" type="checkbox" value="{{  $submenu->id }}"
                                               {{ (in_array($submenu->id, $rights_list, true))? 'checked':'' }} class="input-small"  class="toggle"   name="module[]" />
                                    </td>
                                </tr>
                            @endif
                        @endforeach

        @endif
        @endforeach
    </tbody>

</table>

<div class="form-actions fluid">
    <div class="col-md-offset-3 col-md-9">
        {!! Form::submit('Submit',array('class'=>'btn green')) !!}
        <a href="{{URL::to('/rights')}}"><button class="btn default" type="button">Cancel</button></a>
    </div>
</div>