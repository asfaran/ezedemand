@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Configuration
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Configuration</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{URL::to('/dynamictype')}}">Dynamic Bandwidth Type</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/newstatic')}}">Add New Dynamic Type</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Add New Dynamic Bandwidth Type
                    </div>
                    <div class="tools">
                        <a class="collapse" href=""></a>
                        <a class="reload" href=""></a>
                    </div>
                </div>
                <div class="portlet-body form">

                    {!! Form::open(array('url'=>'storedynamic','role'=>'form', 'class'=>'form-horizontal')) !!}

                    <div class="form-body">
                        <div class="form-group">
                            {!! Form::label('name','Bandwidth Name',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::text('name','',array('id'=>'','class'=>'form-control','placeholder'=>'Enter a Bandwidth  Name')) !!}
                                @if ($errors->has('name'))
                                    <span class="alert-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('price','Price per hour',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::text('price','',array('id'=>'','class'=>'form-control','placeholder'=>'Type a price')) !!}
                                @if ($errors->has('price'))
                                    <span class="alert-danger">{{ $errors->first('price') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('value','Min Value(Mbps)',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::text('value','',array('id'=>'description','class'=>'form-control','placeholder'=>'Add a Min Value for this Bandwidth')) !!}
                                @if ($errors->has('value'))
                                    <span class="alert-danger">{{ $errors->first('value') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('max_value','Max Value(Mbps)',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::text('max_value','',array('id'=>'description','class'=>'form-control','placeholder'=>'Add a Max Value for this Bandwidth')) !!}
                                @if ($errors->has('max_value'))
                                    <span class="alert-danger">{{ $errors->first('max_value') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Save',array('class'=>'btn green')) !!}
                            <a href="{{URL::to('/dynamictype')}}"><button class="btn default" type="button">Cancel</button></a>
                        </div>
                    </div>
                    {!! Form::close() !!}


                </div>
            </div>
        </div>

    </div>
@endsection