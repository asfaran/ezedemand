@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Configuration
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Configuration</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="">Administrator/User</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('new_admin/'.$administrator->id)}}">Update One</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Update a  Administrator/User
                    </div>
                    <div class="tools">
                        <a class="collapse" href=""></a>
                        <a class="reload" href=""></a>
                    </div>
                </div>

                <div class="portlet-body form">
                    {!! Form::open(array('url'=>'update_admin','role'=>'form', 'class'=>'form-horizontal')) !!}

                    {!! Form::hidden('id', $administrator->id, array('id' => 'invisible_id')) !!}

                    <div class="form-body">
                        <div class="form-group">
                            {!! Form::label('name','User Name',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::text('name',($administrator->name)? $administrator->name : '',array('disabled' => 'disabled' , 'id'=>'','class'=>'form-control','placeholder'=>'Enter a New User Name')) !!}
                                @if ($errors->has('name'))
                                    <span class="alert-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        {{--<div class="form-group">
                            {!! Form::label('etisalatid','Etisalat Client ID',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                @if(Auth::user()->access == 2)
                                    {!! Form::text('account_no', ($administrator->account_no)? $administrator->account_no : '' ,array('id'=>'clientid','class'=>'form-control','placeholder'=>'Etisalat Client ID')) !!}
                                @elseif(Auth::user()->access == 1)
                                    {!! Form::text('account_no_disable', ($administrator->account_no)? $administrator->account_no : '' ,array('disabled'=>'disabled','id'=>'clientid','class'=>'form-control','placeholder'=>'Etisalat Client ID')) !!}
                                    {!! Form::hidden('account_no', ($administrator->account_no)? $administrator->account_no : '' , array('id' => 'account_no')) !!}
                                @endif


                                @if ($errors->has('account_no'))
                                    <span class="alert-danger">{{ $errors->first('account_no') }}</span>
                                @endif
                            </div>
                        </div>--}}
                        @if(Auth::user()->id != $administrator->id)
                            <div class="form-group">
                                {!! Form::label('access','User Level',array('id'=>'','class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-9">
                                    {!! Form::select('access',array(''=>'[Select a User Level]', 1 =>'Administrator',0 =>'User'),$administrator->access,array('id'=>'user_access','class'=>'form-control')) !!}
                                    @if ($errors->has('access'))
                                        <span class="alert-danger">{{ $errors->first('access') }}</span>
                                    @endif
                                </div>
                            </div>
                        @endif
                        @if(Auth::user()->access == 2)
                            @if(Auth::user()->id != $administrator->id)
                                <div class="form-group" id="admin_group_area" >
                                    {!! Form::label('groupid','Client Group',array('class'=>'col-md-3 control-label')) !!}
                                    <div class="col-md-9">
                                        {!! Form::select('groupid',$group_array,$administrator->groupid,array('id'=>'groupid','class'=>'form-control')) !!}
                                        @if ($errors->has('groupid'))
                                            <span class="alert-danger">{{ $errors->first('groupid') }}</span>
                                        @endif
                                        @if ($errors->has('user_id'))
                                            <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group" id="admin_gateway_area" >
                                    {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                                    <div class="col-md-4" id="gateway_result">
                                        {!! Form::select('user_id', $gateway_array , $administrator->aliasid, ['id' => 'user_id', 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                            @endif
                        @endif
                        @if(Auth::user()->access == 1)
                            @if(Auth::user()->id != $administrator->id)
                                <div class="form-group"  >
                                    {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                                    <div class="col-md-4" id="gateway_result">
                                        {!! Form::select('user_id', $gateway_array ,$administrator->aliasid, ['id' => 'user_id', 'class' => 'form-control']) !!}
                                        @if ($errors->has('user_id'))
                                            <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endif
                        <div class="form-group">
                            {!! Form::label('mail','Email Address',array('id'=>'','class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    {!! Form::text('email',($administrator->email)? $administrator->email : '' ,array('id'=>'','class'=>'form-control','placeholder'=>'Email Address')) !!}
                                    @if ($errors->has('email'))
                                        <span class="alert-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password','Password',array('id'=>'','class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::password('password',array('id'=>'password', 'class'=>'form-control','placeholder'=>'Password')) !!}
                                    @if ($errors->has('password'))
                                        <span class="alert-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password_confirmation','Confirm Password',array('id'=>'','class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::password('password_confirmation',array('id'=>'password_confirmation','class'=>'form-control','placeholder'=>'Confirm Password')) !!}
                                    @if ($errors->has('password_confirmation'))
                                        <span class="alert-danger">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('contact','Contact Number',array('id'=>'','class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::text('contact',($administrator->contact)? $administrator->contact : '',array('id'=>'contact','class'=>'form-control','placeholder'=>'+971XXXXXX..')) !!}
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Update',array('class'=>'btn green')) !!}
                            <a href="{{URL::to('/administrator')}}"><button class="btn default" type="button">Cancel</button></a>
                        </div>
                    </div>
                    {!! Form::close() !!}


                </div>
            </div>


        </div>
    </div>
@endsection