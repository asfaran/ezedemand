@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Configuration
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Configuration</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/interfaces')}}">Manage Interface</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-picture"></i>Interface list</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="reload" href="javascript:;"></a>
                    </div>
                </div>

                <div class="portlet-body" style="display: block;">
                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif

                    <div class="table-responsive">
                        {!! Form::open(array('url'=>'update_interface','role'=>'form', 'class'=>'form-horizontal')) !!}

                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                                <tr>
                                    <th>Line</th>
                                    <th>Interface</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($interfaces_list  as $interface)
                                <tr>
                                    <td class="highlight">{{$interface->line }}</td>
                                    <td class="hidden-xs">
                                        {!! Form::text('interface['.$interface->id.']',$interface->description,array('class'=>'form-control')) !!}
                                        @if ($errors->has('interface'))
                                            <span class="alert-danger">{{ $errors->first('interface') }}</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="2">
                                    <input class="btn green" type="submit" value="Submit" name="btn_submit">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection