@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Configuration
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Configuration</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/rights')}}">Manage Rights</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                @if ( Session::has('flash_message') )
                    <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                        <button class="close" data-dismiss="alert"></button>
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                @if ( Session::has('flash_success') )
                    <div class="alert alert-success  {{ Session::get('flash_type') }}">
                        <button class="close" data-dismiss="alert"></button>
                        {{ Session::get('flash_success') }}
                    </div>
                @endif
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-picture"></i>Admin Rights</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="reload" href="javascript:;"></a>
                    </div>
                </div>

                <div class="portlet-body col-md-12 " style="display: block;">
                    {!! Form::open(array('url'=>'update_rights','role'=>'form', 'class'=>'form-horizontal')) !!}
                    <div class="form-group">
                            {!! Form::label('user','User Name',array('class'=>'col-md-3 control-label')) !!}
                            <!-- $admin_id -->
                            <div class="col-md-6" >
                                {!! Form::select('rights_user', $user_list,'default' , ['id' => 'rights_user', 'class' => 'form-control']) !!}
                            </div>
                    </div>
                    <div class="clear" ></div>
                    <div class="form-group col-md-12"  id="admin_rights_result">

                    </div>
                    {!! Form::close() !!}

                </div>
            </div>

        </div>

    </div>
@endsection