@extends('app')

@section('content')

    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Configuration
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Configuration</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="">Administrator/User</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/administrator')}}">Manage Admin/User</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-picture"></i>Administrator & User List</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="reload" href="javascript:;"></a>
                    </div>
                </div>

                <div class="portlet-body" style="display: block;">
                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif
                    <div class="table-toolbar">
                        <div class="btn-group">
                            <a href="{{URL::to('/new_admin')}}">
                                <button class="btn green" >
                                    Add New <i class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                        <div class="btn-group pull-right">



                            <button data-toggle="dropdown" class="btn dropdown-toggle">Tools <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="#">Print</a></li>
                                <li><a href="#">Save as PDF</a></li>
                                <li><a href="#">Export to Excel</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>Username</th>
                                {{--<th>Etisalat ID</th>--}}
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Last Seen</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($admin_list  as $admin_list)
                                    <tr>
                                        <td class="highlight">{{$admin_list->name }}</td>
                                       {{-- <td class="hidden-xs">{{$admin_list->account_no }}</td>--}}
                                        <td>{{$admin_list->email }}</td>
                                        <td>{{$admin_list->contact }}</td>
                                        <td>{{$admin_list->updated_at}}</td>
                                        <td>
                                            <a class="btn default btn-xs purple" href="{{URL::to('/new_admin/'.$admin_list->id)}}"><i class="fa fa-edit"></i> Edit</a>
                                            <a class="btn default btn-xs black" href="{{URL::to('/destroy_admin/'.$admin_list->id)}}"><i class="fa fa-trash-o"></i> Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection