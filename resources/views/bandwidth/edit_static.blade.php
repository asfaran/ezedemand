@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Bandwidth
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Manage Bandwidth</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{URL::to('/staticreq')}}">Static Bandwidth</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/newstaticreq/'.$bandwidth_request->id)}}">Update Static Request</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Update Static Bandwidth Request
                    </div>
                    <div class="tools">
                        <a class="collapse" href=""></a>
                        <a class="reload" href=""></a>
                    </div>
                </div>
                <div class="portlet-body form">

                    {!! Form::open(array('url'=>'update_staticreq','role'=>'form', 'class'=>'form-horizontal')) !!}
                    {!! Form::hidden('id', $bandwidth_request->id, array('id' => 'invisible_id')) !!}

                    <div class="form-body">
                        @if(Auth::user()->access == 2 )
                        <div class="form-group">
                            {!! Form::label('groupid','Group',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                {!! Form::select('groupid-disable',$group_array,$bandwidth_request->groupid,array('disabled'=>'disabled','id'=>'groupid','class'=>'form-control')) !!}
                                {!! Form::hidden('groupid', $bandwidth_request->groupid, array('id' => 'invisible_groupid')) !!}
                                @if ($errors->has('groupid'))
                                    <span class="alert-danger">{{ $errors->first('groupid') }}</span><br>
                                @endif
                                @if ($errors->has('user_id'))
                                    <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                                @endif
                                @if ($errors->has('line_id'))
                                    <span class="alert-danger">{{ $errors->first('line_id') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group" >
                            {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4" id="gateway_result">
                                {!! Form::select('user_id', $gateway_array , $bandwidth_request->user_id, ['id' => 'user_id', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        @else
                            <div class="form-group" >
                                {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-4" id="gateway_result">
                                    {!! Form::select('user_id', $gateway_array, $bandwidth_request->user_id, ['id' => 'user_id', 'class' => 'form-control']) !!}
                                    @if ($errors->has('user_id'))
                                        <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                                    @endif
                                    @if ($errors->has('line_id'))
                                        <span class="alert-danger">{{ $errors->first('line_id') }}</span><br>
                                    @endif
                                </div>
                            </div>

                        @endif

                        <div class="form-group" >
                            {!! Form::label('line_id','Line',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4"  id="line_result">
                                {!! Form::select('line_id', $line_array ,$bandwidth_request->line_id, ['id' => 'line_id', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('bandwidth_down','Download Bandwidth',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                <div id="request_bandwidth_down">
                                    {!! Form::select('bandwidth_down',$bandwidth_array,$bandwidth_request->bandwidth_down ,array('id'=>'bandwidth_down','class'=>'form-control')) !!}
                                </div>
                                @if ($errors->has('bandwidth_down'))
                                    <span class="alert-danger">{{ $errors->first('bandwidth_down') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('bandwidth_up','Upload Bandwidth',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                <div id="request_bandwidth_up">
                                     {!! Form::select('bandwidth_up',$bandwidth_array,$bandwidth_request->bandwidth_up,array('id'=>'bandwidth_up','class'=>'form-control')) !!}
                                </div>
                                @if ($errors->has('bandwidth_up'))
                                    <span class="alert-danger">{{ $errors->first('bandwidth_up') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('date_range','Inclusive Date',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                <div class="input-group input-large date-picker input-daterange" data-date="2012-11-12" data-date-format="yyyy-mm-dd">
                                    {!! Form::text('date_start',$bandwidth_request->date_start,array('id'=>'date_start','class'=>'form-control')) !!}
                                    <span class="input-group-addon">to</span>
                                    {!! Form::text('date_end',$bandwidth_request->date_end,array('id'=>'date_end','class'=>'form-control')) !!}

                                </div>
                                <!-- /input-group -->
                                <span class="help-block">Select date range</span>
                                @if ($errors->has('date_start'))
                                    <span class="alert-danger">{{ $errors->first('date_start') }}</span>
                                @endif
                                @if ($errors->has('date_end'))
                                    <span class="alert-danger">{{ $errors->first('date_end') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Update',array('class'=>'btn green')) !!}
                            <a href="{{URL::to('/staticreq')}}"><button class="btn default" type="button">Cancel</button></a>
                        </div>
                    </div>
                    {!! Form::close() !!}


                </div>
            </div>
        </div>

    </div>
@endsection