@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Bandwidth
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Manage Bandwidth</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{URL::to('/dynamicreq')}}">Dynamic Bandwidth</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/newdynamicreq/'.$bandwidth_request->id)}}">Update Dynamic Request</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>Update a Dynamic Bandwidth Request
                    </div>
                    <div class="tools">
                        <a class="collapse" href=""></a>
                        <a class="reload" href=""></a>
                    </div>
                </div>
                <div class="portlet-body form">

                    {!! Form::open(array('url'=>'update_dynamicreq','role'=>'form', 'class'=>'form-horizontal')) !!}
                    {!! Form::hidden('id', $bandwidth_request->id, array('id' => 'invisible_id')) !!}

                    <div class="form-body">
                        @if(Auth::user()->access == 2 )
                            <div class="form-group">
                                {!! Form::label('groupid','Group',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-4">
                                    {!! Form::select('groupid',$group_array,$bandwidth_request->groupid,array('id'=>'groupid','class'=>'form-control')) !!}
                                    @if ($errors->has('groupid'))
                                        <span class="alert-danger">{{ $errors->first('groupid') }}</span><br>
                                    @endif
                                    @if ($errors->has('user_id'))
                                        <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                                    @endif
                                    @if ($errors->has('line_id'))
                                        <span class="alert-danger">{{ $errors->first('line_id') }}</span><br>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-4" id="gateway_result">
                                    {!! Form::select('user_id', $gateway_array, $bandwidth_request->user_id, ['id' => 'user_id', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        @else
                            <div class="form-group">
                                {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-4" id="gateway_result">
                                    {!! Form::select('user_id', $gateway_array , $bandwidth_request->user_id, ['id' => 'user_id', 'class' => 'form-control']) !!}
                                    @if ($errors->has('user_id'))
                                        <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                                    @endif
                                    @if ($errors->has('line_id'))
                                        <span class="alert-danger">{{ $errors->first('line_id') }}</span><br>
                                    @endif
                                </div>
                            </div>

                        @endif
                        <div class="form-group" >
                            {!! Form::label('line_id','Line',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4"  id="line_result">
                                {!! Form::select('line_id', $line_array , $bandwidth_request->line_id, ['id' => 'line_id', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('date_from','Date From',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                <div class="right-inner-addon date"  >
                                    <i class="fa fa-calendar"></i>
                                    {!! Form::text('date_from',$bandwidth_request->date_start,array('id'=>'dynamic_date_from','class'=>'form-control','data-format'=>'yyyy-mm-dd hh:mm')) !!}
                                    @if ($errors->has('date_from'))
                                        <span class="alert-danger">{{ $errors->first('date_from') }}</span>
                                    @endif
                                </div>
                                <!-- /input-group -->
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('date_to','Date To',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                <div class="right-inner-addon date"  >
                                    <i class="fa fa-calendar"></i>
                                    {!! Form::text('date_to',$bandwidth_request->date_end,array('id'=>'dynamic_date_to','class'=>'form-control','data-format'=>'yyyy-mm-dd hh:mm')) !!}
                                    @if ($errors->has('date_to'))
                                        <span class="alert-danger">{{ $errors->first('date_to') }}</span>
                                    @endif
                                </div>
                                <!-- /input-group -->
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Update',array('class'=>'btn green')) !!}
                            <a href="{{URL::to('/dynamicreq')}}"><button class="btn default" type="button">Cancel</button></a>
                        </div>
                    </div>
                    {!! Form::close() !!}


                </div>
            </div>
        </div>

    </div>
@endsection