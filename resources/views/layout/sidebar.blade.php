<!-- BEGIN SIDEBAR -->
<div class="page-sidebar navbar-collapse collapse" style="position:fixed">

    <ul class="page-sidebar-menu">
        <li>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler hidden-phone"></div>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li  {{ Request::is('dashboard') ? ' class=start active' : ' class=start'}}>
            <a href="{{URL::to('/dashboard')}}">
                <i class="fa fa-home"></i>
                <span class="title">Dashboard</span>
            </a>
        </li>
        <li></li>

        <?php
        $menulist = DB::table('menus')->get();
        $current_href = Request::segment(1);

        if($current_href){

            $active_parent = DB::table('menus')
                    ->select('parentid')
                    ->where('href', $current_href )
                    ->first();

            $active_super_parent = DB::table('menus as menu')
                    ->join('menus as mmenu', 'menu.id', '=', 'mmenu.parentid')
                    ->select('menu.parentid')
                    ->where('mmenu.href', $current_href )
                    ->first();

            $active_super_parentid = $active_super_parent->parentid;
            $active_parentid = $active_parent->parentid;

        }else{
            $active_super_parentid = "";
            $active_parentid = "";

        }

        $parent_list = array();
        ?>
        @foreach($menulist as $menu)
            <?php  $parent_list[] = $menu->parentid; ?>
            @if($menu->parentid == false)
                <?php
                if($menu->id == $active_parentid || $menu->id == $active_super_parentid){
                    $mainclass = " class=active " ;
                }else{
                    $mainclass = "  ";
                }
                ?>
                <!--First Main menu list start-->
                <li {{ $mainclass  }}>
                    <a href=''>
                        <i class="{{ $menu->icon }}"></i>
                        <span class="title">{{ $menu->text }}</span>
                        <span class="arrow"></span>
                    </a>
                    <!-- sub menu start -->
                    <ul class='sub-menu' >
                        @foreach($menulist as $submenu)
                            <?php
                            if(Request::is($submenu->href) || $active_parent->parentid == $submenu->id  ){
                                $subclass = ' class=active' ;
                            }else{
                                $subclass = '';
                            }
                            ?>

                            @if($submenu->parentid == $menu->id)
                                <li {{ $subclass  }}>
                                    <a href="{{  URL::to($submenu->href) }}">
                                        <i class="{{  $submenu->icon }}"></i>
                                        {{ $submenu->text }}
                                    </a>

                                    @if(in_array($menu->id, $parent_list))
                                        <?php $childclass = Request::is($childmenu->href)? ' class=active' : '' ; ?>
                                        <ul class='sub-menu'>
                                            @if($childmenu->parentid == $submenu->id)
                                                <li {{ $childclass }}>
                                                    <a href="{{ URL::to($childmenu->href) }}">
                                                        <i class='{{ $childmenu->icon }}'></i>
                                                        {{ $childmenu->text }}
                                                    </a>

                                                </li>
                                            @endif
                                        </ul>
                                    @endif

                                </li>
                            @endif
                        @endforeach
                    </ul>
                    <!-- sub menu finish -->
                </li>
                <!-- First Main menu list end -->

            @endif
        @endforeach

    </ul>
</div>
<!-- END SIDEBAR -->