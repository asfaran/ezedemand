@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Reports
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Reports</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/reportbr')}}">Bandwidth Request</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <form action="" class="form-horizontal" id="myform" method="post">
                <div class="portlet box green paddingless">
                    <div class="portlet-title line">
                        <div class="caption"><i class="fa fa-renren"></i>{!! $report_name !!}</div>

                        <div class="tools">
                            <a href="" class="collapse"></a>
                            <!--<a class="reload" href=""></a>-->
                        </div>
                    </div>
                    <div class="portlet-body" style="">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="note note-success">
                                    <div class="form-group">
                                        <div class="col-md-4" ><strong>Report Type:</strong></div>
                                        <div  class="col-md-8" >{!! $rtype !!}</div>
                                        <div class="col-md-4" ><strong>Period:</strong></div>
                                        <div  class="col-md-8" >{!! $period !!}</div>
                                        <div class="col-md-4" ><strong>Group Name:</strong></div>
                                        <div  class="col-md-8" >{!! $group_name !!}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="table-toolbar">
                                    <div class="btn-group pull-right">
                                        {!! Form::open(array('url'=>'/export_csv','role'=>'form', 'class'=>'form-horizontal')) !!}
                                        {!! Form::close() !!}
                                        {!! Form::open(array('url'=>'/export_csv','role'=>'form', 'class'=>'form-horizontal')) !!}

                                            {!! Form::hidden('start_date',$start_date,array('id'=>'start_date','class'=>'form-control')) !!}
                                            {!! Form::hidden('end_date',$end_date,array('id'=>'end_date','class'=>'form-control')) !!}
                                            {!! Form::hidden('line_id',($line_id)? $line_id : '',array('id'=>'line_id','class'=>'form-control')) !!}
                                            {!! Form::hidden('user_id',$user_id ,array('id'=>'user_id','class'=>'form-control')) !!}
                                            {!! Form::hidden('group_id',$group_id,array('id'=>'group_id','class'=>'form-control')) !!}

                                            {!! Form::submit('Export This Report To CSV',array('class'=>'btn dropdown-toggle yellow')) !!}</br>
                                        {!! Form::close() !!}
                                        {!! Form::open(array('url'=>'/export_pdf','role'=>'form', 'class'=>'form-horizontal')) !!}
                                            {!! Form::submit('Export This Report To PDF',array('class'=>'btn dropdown-toggle green')) !!}

                                            {!! Form::hidden('start_date',$start_date,array('id'=>'start_date','class'=>'form-control')) !!}
                                            {!! Form::hidden('end_date',$end_date,array('id'=>'end_date','class'=>'form-control')) !!}
                                            {!! Form::hidden('line_id',($line_id)? $line_id : '',array('id'=>'line_id','class'=>'form-control')) !!}
                                            {!! Form::hidden('user_id',$user_id ,array('id'=>'user_id','class'=>'form-control')) !!}
                                            {!! Form::hidden('group_id',$group_id,array('id'=>'group_id','class'=>'form-control')) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <table class="table  table-hover table-bordered " id="reports_table" >
                            <thead>
                            <tr>

                               @if($report_type == 'subscriber')
                                    <th>Account Number</th>
                                    <th>Group Name</th>
                                    <th>Customer Name</th>
                                    <th>Line</th>
                                    <th>Activation Date</th>
                                    <th>Deactivation Date</th>
                                    <th>Product</th>
                                    <th>Quantity</th>

                                @elseif($report_type == 'revenue')

                                    <th>Line</th>
                                    <th>Activation Date</th>
                                    <th>Deactivation Date</th>
                                    <th>Product</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Total</th>

                                @elseif($report_type == 'usage')

                                    <th>Activation Date</th>
                                    <th>Deactivation Date</th>
                                    <th>Product</th>
                                    <th>Quantity</th>

                                @endif

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($request_list  as $request)

                                <?php

                                if ($request->burst == 1) {
                                    $product_type = "Dynamic";
                                    $diff = abs(strtotime($end_date) - strtotime($request->date_start));
                                    $quantity = floor($diff / (60 * 60));
                                } else {
                                    $product_type = "Static";
                                    $diff = abs(strtotime($end_date) - strtotime($request->date_start));
                                    $quantity = floor($diff / (60 * 60 * 24));
                                }

                                $total_price = $quantity * $request->Unit_Price;

                                    if($report_type == 'usage' && $request->burst == 1){

                                    $start_date = date('Y-m-d H:i:s', strtotime( $request->date_start));
                                    $end_date = date('Y-m-d H:i:s', strtotime($end_date));

                                    }elseif($report_type == 'usage' && $request->burst == 0){
                                    $start_date = date('Y-m-d', strtotime($request->date_start));
                                    $end_date = date('Y-m-d', strtotime($end_date));


                                    }elseif($report_type == 'revenue'){

                                    $start_date = date('Y-m-d H:i:s', strtotime($request->date_start));
                                    $end_date = date('Y-m-d H:i:s', strtotime($end_date));

                                    }elseif($report_type == 'subscriber'){

                                    $start_date = date('Y-m-d', strtotime($request->date_start));
                                    $end_date = date('Y-m-d', strtotime($end_date));
                                    }
                                ?>

                                <tr>
                                    @if($report_type == 'subscriber')

                                    <td>{!! $request->accno  !!}</td>
                                    <td>{!!  $request->group_name  !!}</td>
                                    <td>{!!  $request->client_name  !!}</td>
                                    <td>{!!  $request->interface  !!}</td>
                                    <td>{!!  $start_date !!}</td>
                                    <td>{!!  $end_date !!}</td>
                                    <td>{!!  $product_type."-".$request->bod_name !!}</td>
                                    <td>{!!  $quantity !!}</td>



                                    @elseif($report_type == 'revenue')

                                    <td>{!!  $request->interface !!}</td>
                                    <td>{!!  $start_date !!}</td>
                                    <td>{!!  $end_date !!}</td>
                                    <td>{!!  $product_type."-".$request->bod_name !!}</td>
                                    <td>{!!  $quantity !!}</td>
                                    <td>{!! $request->Unit_Price !!}</td>
                                    <td>{!! $total_price !!}</td>



                                    @elseif($report_type == 'usage')

                                    <td>{!!  $start_date !!}</td>
                                    <td>{!!  $end_date !!}</td>
                                    <td>{!!  $product_type."-".$request->bod_name  !!}</td>
                                    <td>{!!  $quantity  !!}</td>
                                    @endif

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection