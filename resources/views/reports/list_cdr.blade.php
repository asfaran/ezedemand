@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Reports
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Reports</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/list_cdr')}}">List CDR Reports</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-picture"></i>List of Generated CDR Reports</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="reload" href="javascript:;"></a>
                    </div>
                </div>

                <div class="portlet-body" style="display: block;">
                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif
                    <div class=" display-none">
                        <button class="close" data-dismiss="alert"></button>
                        Your form validation is successful!
                    </div>

                    <div class="table-toolbar">
                        <div class="btn-group">
                            <a href="{{URL::to('/reports_cron')}}">
                                <button class="btn green" >
                                    Generate New CDR <i class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>CDR Name</th>
                                <th>Size</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($cdr_list as $file)
                                <tr>
                                    <td>{!! (string)$file->getRelativePathName() !!}</td>
                                    <td>{!! (string)$file->getSize() !!} bytes</td>
                                    <td>
                                        <a href="reports/{{ $file->getRelativePathName() }}" ><button class="btn green" type="button">Download</button></a>
                                        <a href="{{URL::to('/destroy_cdr/'.$file->getRelativePathName())}}"><button class="btn default" type="button">Delete</button></a>
                                    </td>

                                    {{--link_to($file, 'Link')--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection