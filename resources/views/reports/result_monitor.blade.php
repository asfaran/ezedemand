@extends('app-reports')

<script>

  var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
  var lineChartData = {
    labels : <?php  print json_encode(array_reverse($label)); ?>,
    datasets : [
      {
        label: "Static Download",
        fillColor : "rgba(220,220,220,0.2)",
        strokeColor : "rgba(220,220,220,1)",
        pointColor : "rgba(220,220,220,1)",
        pointStrokeColor : "#fff",
        pointHighlightFill : "#fff",
        pointHighlightStroke : "rgba(220,220,220,1)",
        data : <?php  print json_encode(array_reverse(array_values($result_static))); ?>
      },
      {
        label: "Burstable Download",
        fillColor: "rgba(151,187,205,0.2)",
        strokeColor: "rgba(151,187,205,1)",
        pointColor: "rgba(151,187,205,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(151,187,205,1)",
        data: <?php  print json_encode(array_reverse(array_values($result_dynamic))); ?>
      }
    ]

  }

  function line_ChartData(){

    var static_status = document.getElementById("result_static");
    var dynamic_status = document.getElementById("result_dynamic");

    if((dynamic_status.checked == 1) && (static_status.checked == 1)){

      var lineChartData = {
        labels : <?php  print json_encode(array_reverse($label)); ?>,
        datasets : [
          {
            label: "Static Download",
            fillColor : "rgba(220,220,220,0.2)",
            strokeColor : "rgba(220,220,220,1)",
            pointColor : "rgba(220,220,220,1)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(220,220,220,1)",
            data : <?php  print json_encode(array_reverse(array_values($result_static))); ?>
          },
          {
            label: "Burstable Download",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: <?php  print json_encode(array_reverse(array_values($result_dynamic))); ?>
          }
        ]

      }

    }else if((dynamic_status.checked == 1 ) && (static_status.checked == 0 )){
      var lineChartData = {
        labels : <?php  print json_encode(array_reverse($label)); ?>,
        datasets : [
          {
            label: "Static Download",
            fillColor : "rgba(220,220,220,0.2)",
            strokeColor : "rgba(220,220,220,1)",
            pointColor : "rgba(220,220,220,1)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(220,220,220,1)",
            data : <?php  print json_encode(array_reverse(array_values($result_static))); ?>
          }
        ]

      }

    }else if((dynamic_status.checked == 0 ) && (static_status.checked == 1)){
      var lineChartData = {
        labels : <?php  print json_encode(array_reverse($label)); ?>,
        datasets : [
          {
            label: "Burstable Download",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: <?php  print json_encode(array_reverse(array_values($result_dynamic))); ?>
          }
        ]

      }

    }

    if(document.getElementById("canvas-monthly").getAttribute("class") == 'canvas-monthly'){
      var ctx = document.getElementById("canvas-monthly").getContext("2d");
      window.myLine = new Chart(ctx).Line(lineChartData, {
        responsive: true
      });
    }

  }


  window.onload = function(){
    if(document.getElementById("canvas-monthly").getAttribute("class") == 'canvas-monthly'){
      var ctx = document.getElementById("canvas-monthly").getContext("2d");
      window.myLine = new Chart(ctx).Line(lineChartData, {
        responsive: true
      });
    }
  }
</script>
@section('content')
  <!-- BEGIN PAGE HEADER-->
  <div class="row">
    <div class="col-md-12">
      <!-- BEGIN PAGE TITLE & BREADCRUMB-->
      <h3 class="page-title">
        Reports
      </h3>
      <ul class="page-breadcrumb breadcrumb">
        <li>
          <i class="fa fa-cogs"></i>
          <a href="#">Reports</a>
          <i class="fa fa-angle-right"></i>
        </li>
        <li><a href="{{URL::to('/bdmonitor')}}">Bandwidth Monitor</a></li>
      </ul>
      <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
  </div>
  <!-- END PAGE HEADER-->
  <div class="row">
    <div class="col-md-12">

      <div style="clear: both"></div>

      <div class="portlet box blue">

        <div class="portlet-title">
          <div class="caption"><i class="fa fa-edit"></i>Bandwidth Usage Monitor</div>
          <div class="tools">
            <a href="javascript:;" class="collapse"></a>
            <a href="javascript:;" class="reload"></a>
          </div>
        </div>
        <div class="portlet-body">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-10">
                <h3>
                  @if($report_types == 'hours')
                    'Last 24 Hours Record'
                  @elseif($report_types == 'dailyweek')
                    'Daily for week'
                  @elseif($report_types == 'dailymonth')
                    'Daily for month'
                  @elseif($report_types == 'monthly')
                    'Monthly'
                  @endif

                  Monitoring (Max Speed in Mbps)</h3>
                <canvas id="canvas-monthly"   class="canvas-monthly"></canvas>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="checkbox-list">
                <label>
                  <input type="checkbox" id="result_static" onchange="line_ChartData();"><span style="background-color:rgba(220,220,220,1);text-indent: 2em "> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Static</span>
                </label>
                <label>
                  <input type="checkbox" id="result_dynamic"  onchange="line_ChartData();"><span style="background-color:rgba(151,187,205,1);text-indent: 3em ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dynamic</span>
                </label>
              </div>
              <?php //echo $sql; ?>
            </div>
          </div>
          <div class="row">
            <div class=" form-actions fluid">
              <div class="col-md-9"></div>
              <div class="col-md-3">
                <a href="{{URL::to('/bdmonitor')}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to clear monitoring  data?')" title="Clear current data">Back</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection