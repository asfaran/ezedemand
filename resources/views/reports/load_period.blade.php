@if($report_type == 'usage' || $report_type == 'revenue' )

    <label class="control-label col-md-3" for="inputSuccess">Month</label>
    <div  class="col-md-4" >
        <select name="month" class="form-control" id="month">
            <option value=''>Select a Month</option>
            <?php
            for ($x = 1; $x <= 12; $x++) {
                $value = date("Y-m",strtotime("-".$x." Months"));
                $data = date("Y-F",strtotime("-".$x." Months"));
                echo "<option value='".$value."'>".$data."</option>";
            }
            ?>
        </select>
    </div>


@else

    {!! Form::label('date_range','Date Range',array('class'=>'control-label col-md-3')) !!}
    <div class="col-md-4">
        <div class="input-group input-large date-picker input-daterange" data-date="2012-11-12" data-date-format="yyyy-mm-dd">
            {!! Form::text('date_start','',array('id'=>'date_start','class'=>'form-control')) !!}
            <span class="input-group-addon">to</span>
            {!! Form::text('date_end','',array('id'=>'date_end','class'=>'form-control')) !!}

        </div>
        <!-- /input-group -->
        <span class="help-block">Select date range</span>
        @if ($errors->has('date_start'))
            <span class="alert-danger">{{ $errors->first('date_start') }}</span>
        @endif
        @if ($errors->has('date_end'))
            <span class="alert-danger">{{ $errors->first('date_end') }}</span>
        @endif
    </div>



@endif