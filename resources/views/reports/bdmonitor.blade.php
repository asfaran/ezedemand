@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Reports
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Reports</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/bdmonitor')}}">Bandwidth Monitor</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-picture"></i>Bandwidth Monitor Reports</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="reload" href="javascript:;"></a>
                    </div>
                </div>

                <div class="portlet-body " style="display: block;">
                    {!! Form::open(array('url'=>'generate_bdmonitor','role'=>'form', 'class'=>'form-horizontal')) !!}
                    <div class="col-md-12 form-group">
                        {!! Form::label('report_types','Select Report Type',array('class'=>' control-label')) !!}
                        {!! Form::select('report_types', array(''=>'[Select a Report Type]','dailyweek' =>'Daily for Week', 'dailymonth'=>'Daily for Month', 'monthly'=>'Monthly'), 'default', ['id' => 'report_types', 'class' => 'form-control']) !!}
                        @if ($errors->has('report_types'))
                            <span class="alert-danger">{{ $errors->first('report_types') }}</span>
                        @endif
                    </div>
                    @if(Auth::user()->access == 2)
                        <div class="form-group">
                            {!! Form::label('groupid','Group',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                {!! Form::select('groupid',$group_array,'enabled',array('id'=>'groupid','class'=>'form-control')) !!}
                                @if ($errors->has('groupid'))
                                    <span class="alert-danger">{{ $errors->first('groupid') }}</span><br>
                                @endif
                                @if ($errors->has('user_id'))
                                    <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                                @endif
                                @if ($errors->has('line_id'))
                                    <span class="alert-danger">{{ $errors->first('line_id') }}</span><br>
                                @endif
                            </div>
                        </div>
                        <div class="form-group" id="gateway_area" style="display: none">
                            {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4" id="gateway_result">
                                {!! Form::select('user_id', $gateway_array , null, ['id' => 'user_id', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                    @endif
                    @if(Auth::user()->access == 1)
                    <div class="form-group">
                        {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-4" id="gateway_result">
                            {!! Form::select('user_id',$gateway_array , null, ['id' => 'user_id', 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    @endif
                    <div class="form-group"  id="line_area" style="display: none">
                        {!! Form::label('line_id','Line',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-4"  id="line_result">
                            {!! Form::select('line_id', array(''=>'[Pls.Select Group & Gateway First]'), null, ['id' => 'line_id', 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Generate Report',array('class'=>'btn green')) !!}
                            <a href="{{URL::to('/bdmonitor')}}"><button class="btn default" type="button">Cancel</button></a>
                        </div>
                    </div>

                    <div class="form-group"  id="admin_rights_result">

                    </div>
                    {!! Form::close() !!}

                </div>
            </div>

        </div>

    </div>
@endsection