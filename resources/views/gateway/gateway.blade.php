@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Gateway
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Manage Gateway</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{URL::to('/gateway')}}">Gateways List</a>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-picture"></i>Gateways List</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="reload" href="javascript:;"></a>
                    </div>
                </div>

                <div class="portlet-body" style="display: block;">
                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif
                    <div class="table-toolbar">
                        <div class="btn-group">
                            &nbsp;
                        </div>
                        <div class="btn-group pull-right">
                            <button data-toggle="dropdown" class="btn dropdown-toggle">Filter List<i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{URL::to('/gateway/')}}"><?php if($url_id == ""){ echo '<span class="fa fa-check"></span>'; }  ?>All</a></li>
                                <li><a href="{{URL::to('/gateway/1')}}"><?php if($url_id == 1){ echo '<span class="fa fa-check"></span>'; }  ?>Online</a></li>
                                <li><a href="{{URL::to('/gateway/0')}}"><?php if($url_id == '0'){ echo '<span class="fa fa-check"></span>'; }  ?>Offline</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="table-responsive">
                        {!! Form::open(array('url'=>'update_lines','role'=>'form', 'class'=>'form-vertical')) !!}
                            <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                <tr>
                                    <th><span  class="fa fa-arrow-down"></span></th>
                                    <th>Gateway Name</th>
                                    <th>Mac Address</th>
                                    <th>Group</th>
                                    <th>Status</th>
                                    <th>Last Seen</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($gateway_list )
                                    @foreach($gateway_list  as $gateway)

                                        <?php
                                        $dt = new DateTime();
                                        $current_dt = $dt->format('Y-m-d h:i:s');

                                        $seconds  = strtotime($current_dt) - strtotime($gateway->updated_at);
                                        $diff_time = floor($seconds / 60);

                                        ?>
                                        <tr>
                                            <td id="{{$gateway->id }}">
                                                <span  class=" fa fa-plus gateway-row-plus"></span>
                                                <span  class="fa fa-minus gateway-row-minus"></span>
                                            </td>
                                            <td class="highlight">{{ $gateway->alias }}</td>
                                            <td class="hidden-xs">{{$gateway->mac }}</td>
                                            <td>{{$gateway->name }}</td>
                                            <td>{!! ($diff_time <= 5 )? "<span class='badge badge-success'>Online</span>" : "<span class='badge badge-warning'>Offline</span>"  !!}</td>
                                            <td>{{$gateway->updated_at }}</td>
                                            <td>

                                                @if(Auth::user()->access == 2)
                                                    <a class="btn default btn-xs purple" href="{{URL::to('/edit_gateway/'.$gateway->id)}}"><i class="fa fa-edit"></i> Edit</a>
                                                    <a class="btn default btn-xs black" href="{{URL::to('/destroy_gateway/'.$gateway->id)}}"><i class="fa fa-trash-o"></i> Delete</a>
                                                @else
                                                    <i class="fa fa-edit"></i> Edit
                                                    <i class="fa fa-trash-o"></i> Delete
                                                @endif

                                            </td>
                                        </tr>
                                        <tr id="datarow-{{$gateway->id }}"  style="display: none">
                                            <td colspan="7" id="gateway_result{{$gateway->id }}">

                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">No data for this...</td>
                                    </tr>

                                @endif
                                </tbody>
                            </table>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>


    </div>
@endsection