jQuery(document).ready(function() {

    var csrfToken = $("input[name=_token]").val();

$.ajaxPrefilter(function(options, originalOptions, jqXHR) {
    if (options.type.toLowerCase() === 'post')
    {
        options.data += options.data?'&':''; // add leading ampersand if `data` is non-empty
        options.data += '_token=' + csrfToken; // add _token entry
    }
});

/**
Custom module for you to write your own javascript functions
**/
var Custom = function () {

    // private functions & variables

    var myFunc = function(text) {
        alert(text);
    }

    // public functions
    return {

        //main function
        init: function () {
            //initialize here something.            
        },

        //some helper function
        doSomeStuff: function () {
            myFunc();
        }

    };

}();

/***
Usage
***/
//Custom.init();
//Custom.doSomeStuff();





    // initiate layout and plugins
    App.init();
    FormComponents.init();


    $('.gateway-row-minus').hide();
    $( ".gateway-row-plus" ).click(function() {
        var rowid= $( this).closest("td").attr('id');
        $.ajax({
            type: "POST",
            url: "load_linelist",
            data: { 'alias_id' : rowid },
            dataType: 'html',
            success: function (data) {
                $('#datarow-'+rowid).show();
                $('#'+rowid+' .gateway-row-plus').hide();
                $('#'+rowid+' .gateway-row-minus').show();
                $("#gateway_result"+rowid).html(data);
            }
        });




    });

    $( ".gateway-row-minus" ).click(function() {
        var rowid= $( this).closest("td").attr('id');
        $('#datarow-'+rowid).hide();
        $('#'+rowid+' .gateway-row-minus').hide();
        $('#'+rowid+' .gateway-row-plus').show();
    });


    /* new date range start ,,, */

    var startDate = new Date();
    var FromEndDate = new Date();
    var ToEndDate = new Date();


    ToEndDate.setDate(ToEndDate.getDate()+365);
    //$('body').on('click', 'input.datepicker', function(event)
    $('#filter_date_start').datepicker({
        autoclose: true,
        endDate: ToEndDate,
        dateFormat: 'yy-mm-dd'
    })
        .on('changeDate', function(selected){
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('#filter_date_end').datepicker('setStartDate', startDate);
        });
    $('#filter_date_end').datepicker({

        endDate: ToEndDate,
        startDate: startDate,
        dateFormat: 'yy-mm-dd',
        autoclose: true
    })
        .on('changeDate', function(selected){
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('#filter_date_start').datepicker('setEndDate', FromEndDate);
        });


    $('#country').change(function(){

        $.ajax({
            type: "POST",
            url: "/loadcities",
            data: { 'country_id' : $(this).val() },
            dataType: 'html',
            success: function (data) {
                $("#city_result").html(data);
            }
        });

    });

    $('#dynamic_date_from').datetimepicker({
        startDate: startDate,
        autoclose: true,
        minuteStep:60,
        culture: "bg-BG",
        maxDate: false,
        use24hours: true,
        format : 'yyyy-mm-dd hh:ii'
    })
        .on('changeDate', function(selected){

            /*startDate = new Date(selected.date.valueOf());*/

            var startDate = new Date(selected.date.valueOf() + new Date().getTimezoneOffset()*60*1000 + 1*59*60*1000);

            /*startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));*/
            $('#dynamic_date_to').datetimepicker('setStartDate', startDate );
        });
    $('#dynamic_date_to').datetimepicker({
        startDate: startDate,
        autoclose: true,
        culture: "bg-BG",
        minuteStep:59,
        maxDate: false,
        use24hours: true,
        format : 'yyyy-mm-dd hh:ii'
    })
        .on('changeDate', function(selected){
            FromEndDate = new Date(selected.date.valueOf());
            /*FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));*/
            $('#dynamic_date_from').datetimepicker('setEndDate', FromEndDate);
        });
    /* //new date range end ,,,  */

    $('#gateway_area').hide();
    $('#line_area').hide();


    $('#user_access').change(function(){
       if($(this).val() == 1){
           $('#admin_group_area').show();
           $('#admin_gateway_area').hide();

       }else{
           $('#admin_group_area').show();
           $('#admin_gateway_area').show();

       }

    });



    $('#groupid').change(function(){
        $.ajax({
            type: "POST",
            url: "load_gateways",
            data: { 'group_id' : $(this).val() },
            dataType: 'html',
            success: function (data) {
                $('#gateway_area').show();
                $('#line_area').hide();
                $("#gateway_result").html(data);
            }
        });

    });




    if($('#user_id').val()){
        $.ajax({
            type: "POST",
            url: "load_lines",
            data: { 'user_id' :  $(this).val()},
            dataType: 'html',
            success: function (data) {
                $('#line_area').show();
                $("#line_result").html(data);
            }
        });
    }

    $( "body" ).on( "change", "#br_report_types", function() {
        if( $(this).val() != ""){
            if($(this).val() == 'usage' || $(this).val() == 'revenue' ){
                $('#month_select').show();
                $('#date_select').hide();

            }else{
                $('#date_select').show();
                $('#month_select').hide();
            }

        }else{
            $('#reportbr_period').hide();
        }
    });



    $( "body" ).on( "change", "#rights_user", function() {
        $.ajax({
            type: "POST",
            url: "load_rights",
            data: { 'admin_id' : $(this).val()},
            dataType: 'html',
            success: function (data) {
                $("#admin_rights_result").html(data);
            }
        });
    });



    $( "body" ).on( "change", "#user_id", function() {
        $.ajax({
            type: "POST",
            url: "load_lines",
            data: { 'user_id' : $(this).val()},
            dataType: 'html',
            success: function (data) {
                $('#line_area').show();
                $("#line_result").html(data);
            }
        });
    });


    $( "body" ).on( "change", ".line_default_down", function() {
        if( $(this).val() >0 ){

            var rowid= $( this).closest("tr").attr('rowid');
            $.ajax({
                type: "POST",
                url: "/lines_maxdown",
                data: { 'default_down' : $(this).val()},
                dataType: 'html',
                success: function (data) {
                    $("#line_max_down_"+rowid).html(data);
                }
            });

        }
    });




    $( "body" ).on( "change", ".line_default_up", function() {
        if( $(this).val() >0 ){

            var rowid= $( this).closest("tr").attr('rowid');
            $.ajax({
                type: "POST",
                url: "/lines_maxup",
                data: { 'default_up' : $(this).val()},
                dataType: 'html',
                success: function (data) {
                    $("#line_max_up_"+rowid).html(data);
                }
            });

        }
    });


    $( "body" ).on( "change", "#request_line_id", function() {
        if( $(this).val() >0 ){

           $.ajax({
                type: "POST",
                url: "load_bandwidth",
                data: { 'line_id' : $(this).val()},
                dataType: 'json',
                success: function (data) {
                    $("#request_bandwidth_down").html(data.down);
                    $("#request_bandwidth_up").html(data.up);
                }
            });

        }
    });








});